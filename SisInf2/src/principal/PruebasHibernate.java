package principal;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbdd.Categorias;
import bbdd.Empresas;
import bbdd.Nomina;
import bbdd.Trabajadorbbdd;

public class PruebasHibernate{

	private static Session session;
	
	public static void main(String[] args) {
		SessionFactory sf = null;
		sf=HibernateUtils.getSessionFactory();
		session = sf.openSession();
		
		Transaction tx = session.beginTransaction();
		
		Categorias cat = new Categorias("CatNombre", 1245.85, 152.5);
		Empresas empresas = new Empresas("Empresita", "45454545K");
		Trabajadorbbdd trab = new Trabajadorbbdd();
		System.out.println(cat.getNombreCategoria());
		System.out.println(empresas.getCif());
		
		session.save(cat);
		session.save(empresas);
		
		tx.commit();

		session.close();
		System.out.println("Done");
	}
}
