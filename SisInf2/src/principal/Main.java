package principal;

import java.io.IOException;
import java.util.Scanner;

import objetos.Fecha;

public class Main {
	
	public static void main(String[] args) throws IOException {
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Esta aplicaci�n realizar� varias funciones:");
		System.out.println("\t�Comprobar la valided de los DNI de los empleados, corrigiendo los erroneos");
		System.out.println("\t�Identificar la duplicidad de los datos de los empleados (Dos empleados con el mismo DNI)");
		System.out.println("\t�Generar de manera automatica direcciones de correo electr�nico para los empleados");
		System.out.println("\t�Identificar y corregir numeros de cuenta bancaria incorrectos");
		System.out.println("\t�Generar un numero de IBAN correcto a partir del numero de cuenta bancaria corregido");
		System.out.println("\t�Generar n�minas en formato PDF para una fecha dada");
		System.out.println("\t�Exportar todos los datos de los empleados, empresas, categorias y nominas generadas a una base de datos");
		System.out.println("");
		System.out.print("Para comenzar, introduzca la fecha de la que quiere generar las n�minas, utilizando el formato MM/AAAA: ");
		
		String fechaIntroducida = teclado.nextLine();
		if(fechaIntroducida.length() != 7) {
			System.out.println("No se ha introducido la fecha en el formato esperado, por favor reinicie la aplicaci�n y vuelva a intentarlo");
			System.exit(0);
		}
		System.out.println("");
		int mes = Integer.parseInt(fechaIntroducida.split("/")[0]);
		int anio = Integer.parseInt(fechaIntroducida.split("/")[1]);
		Fecha fecha = new Fecha(1,mes,anio);
		Nucleo.getInstancia().run(fecha);
	}
}