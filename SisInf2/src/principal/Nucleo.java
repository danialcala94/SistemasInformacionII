package principal;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;

import org.apache.commons.collections4.Trie;
import org.apache.commons.collections4.keyvalue.TiedMapEntry;
import org.apache.lucene.index.ExitableDirectoryReader;
import org.hibernate.query.criteria.internal.predicate.ExistsPredicate;

import bbdd.Categorias;
import bbdd.Empresas;
import bbdd.Trabajadorbbdd;
import entradasalida.Exportar;
import entradasalida.Importar;
import entradasalida.exportacionBD;
import objetos.Cuota;
import objetos.Empleado;
import objetos.Fecha;
import objetos.Nomina;
import objetos.Retencion;
import objetos.SalarioCategoria;
import objetos.Trienio;
import utilidades.Constantes;
import utilidades.NifNie;
import utilidades.Utilidades;
import utilidades.calcularNominas;
import utilidades.conversoresBD;
import utilidades.nominasDefensa;

/**
 * (SINGLETON)
 * Clase que controla la aplicaci�n en general.
 * @author GAMING
 *
 */
public class Nucleo {
	
	/**
	 * Lista de empleados que son le�dos del EXCEL (informaci�n bruta).
	 */
	private ArrayList<Empleado> empleados = new ArrayList<Empleado>();
	/**
	 * Lista de empleados que contienen errores y deben exportarse a un fichero XML.
	 */
	private ArrayList<Empleado> empleadosXML = new ArrayList<Empleado>();
	/**
	 * Lista de empleados que contienen errores en la cuenta bancaria y deben exportarse a un fichero XML.
	 */
	private ArrayList<Empleado> empleadosErroresCuentas = new ArrayList<Empleado>();
	/**
	 * Lista de empleados con la informaci�n corregida y que deben ser actualizados en el EXCEL.
	 */
	private ArrayList<Empleado> empleadosCorregidos = new ArrayList<Empleado>();
	
	private ArrayList<Cuota> listaCuotas = new ArrayList<Cuota>();
	
	private ArrayList<Retencion> listaRetenciones = new ArrayList<Retencion>();
	
	private ArrayList<SalarioCategoria> listaSalariosCategoria = new ArrayList<SalarioCategoria>();
	
	private ArrayList<Trienio> listaTrienios = new ArrayList<Trienio>();
	
	private ArrayList<Nomina> listaNominas = new ArrayList<Nomina>();
	
	private ArrayList<Empresas> listaEmpresas = new ArrayList<Empresas>();
	
	ArrayList<Empleado> listaEmpleadosParaNominas = new ArrayList<Empleado>();
	
	
	
	private static final Nucleo instancia = new Nucleo();
	
	
	public Nucleo() {
		// Singleton
	}
	
	/**
	 * Devuelve la lista de empleados que han sido le�dos del EXCEL (informaci�n bruta).
	 * @return empleados
	 */
	public ArrayList<Empleado> getEmpleados() {
		return this.empleados;
	}
	/**
	 * Devuelve la lista de empleados que contienen errores y deben exportarse a un fichero XML.
	 * @return empleadosXML
	 */
	public ArrayList<Empleado> getEmpleadosXML() {
		return this.empleadosXML;
	}
	/**
	 * Devuelve la lista de empleados cuya informaci�n ha sido corregida y deben ser actualizados en el EXCEL.
	 * @return empleadosCorregidos
	 */
	public ArrayList<Empleado> empleadosCorregidos() {
		return this.empleadosCorregidos;
	}
	
	/**
	 * Devuelve al instancia de la clase si ya est� iniciada, o inicia una nueva.
	 * @return
	 */
	public static Nucleo getInstancia() {
		return instancia;
	}
	
	/**
	 * Devuelve true si la clase est� instanciada, o false si no.
	 * @return
	 */
	public boolean estaInstanciado() {
		if (instancia != null)
			return true;
		return false;
	}
	
	/**
	 * Ejecuta la funcionalidad de la aplicaci�n.
	 */
	public void run(Fecha fecha) {
		ejecutarPractica3();
		exportarXMLyEXCEL();
		
		System.out.println("\nSe van a generar las n�minas correspondientes a la fecha indicada\n");
		generaNominas(fecha);
		//Exportar las nominas a PDF
		//Despues de haber generado las nominas, la lista nominas contendr� las nominas de los empleados para esa fecha
		//La funicon generar PDF se puede comentar para que en la presentacion de la aplicacion tarde menos en ejecutarse
		
		System.out.println("\n:::::::Se ha comentado la exportacion a PDF las nominas::::\n");
		Exportar.generarPDF(listaNominas);	
		
		
		
		
		System.out.println("Se va a comenzar con la exportaci�n a base de datos\n");
		/*Parte de la exportacion a Base de datos*/
		//Obtenemos una lista de todas las empresas de la aplicacion
		listaEmpresas = Utilidades.getListaEmpresas(listaEmpleadosParaNominas);
		//Exportamos las empresas
		exportacionBD.exportarEmpresas(listaEmpresas);
		//Exportamos las categorias
		ArrayList<Categorias> listaCategorias = conversoresBD.conversorCategoria(listaSalariosCategoria);
		exportacionBD.exportarCategorias(listaCategorias);
		//Exportamos los empleados
		ArrayList<Trabajadorbbdd> listaTrabajadoresbbdd = conversoresBD.conversorEmpleado(listaEmpleadosParaNominas);
		exportacionBD.exportarTrabajadores(listaTrabajadoresbbdd);
		//Exportamos las nominas
		ArrayList<bbdd.Nomina> listaNominasBD = conversoresBD.conversorNomina(listaNominas);		
		exportacionBD.exportarNominas(listaNominasBD);
		
		
		//Despues de exportar a bbdd Obtenemos la nomina con menor liquido de empleado de la lista de listaNominasBD
		bbdd.Nomina nominaMinC = nominasDefensa.nominaLiquidoMin(listaNominasBD);
		bbdd.Nomina nominaMinBD = nominasDefensa.nominaLiquidoMinBD();
		
		Exportar.generarPDFDefensa(nominaMinC, nominaMinBD);
		
		
		System.out.println("\n\nLa aplicaci�n ha terminado su ejecuci�n");
		/*Fin de la aplicacion*/
		System.exit(0);
	}
	
	public void ejecutarPractica3(){
		try {
			Importar.getInstancia().leeHoja1Excel(empleados);
		} catch (IOException e) {
			System.err.println("No se ha podido abrir el archivo Excel, por favor compruebe que est� ubicado en la carpeta resources y su nombre es el correcto");
			System.err.println("No se continuar� la ejecuci�n del programa");
			System.exit(0);
		}
		ArrayList <Empleado> empleadosOriginales = (ArrayList<Empleado>) empleados.clone();
		empleados = NifNie.compruebaDNIVacio(empleados, empleadosXML);
		NifNie.compruebaDNICorrecto(empleados, empleadosXML, empleadosCorregidos);
		NifNie.compruebaDNIRepetido(empleados, empleadosXML);
		System.out.println("Se han comprobado los DNI incorrectos y duplicados de los empleados");
		
		//A partir de este punto, todos los empleados est�n en la lista de empleados Corregidos asique se puede trabajar directamente con esta
		utilidades.ComprobacionesEmail.completaEmailsEmpleados(empleadosOriginales, empleadosCorregidos);
		System.out.println("Se han generado las direcciones de email para los empleados");
		//System.out.println("Comienza");
		utilidades.OperacionesBancarias.rellenaIBAN_Y_CuentaBancaria(empleadosCorregidos,empleadosErroresCuentas);
		System.out.println("Se han comprobado/corregido los numeros de cuenta bancaria y generados los IBAN");
		
	}
	
	public void exportarXMLyEXCEL() {
		ArrayList<Empleado> exportarEXCEL = new ArrayList<Empleado>();
		for (int i = 0; i < empleadosCorregidos.size(); i++) {
			if (empleadosCorregidos.get(i).getAtributo("IBAN") != null) {
				exportarEXCEL.add(new Empleado(empleadosCorregidos.get(i).getNif(), empleadosCorregidos.get(i).getNumeroFila(), empleadosCorregidos.get(i).getEmail(), empleadosCorregidos.get(i).getCuentaBancaria(), empleadosCorregidos.get(i).getAtributo("IBAN").getValorAtributo().toString()));
			}
		}
		;
		
		//TODO
		//Esto debe ser lo ultimo que se haga, que es exportar el XML y el Excel, todas las operaciones se tienen que hacer antes
		try {
			Exportar.getInstancia().exportarXML(1, Constantes.FICHERO_XML_ERRORES, "Trabajadores", "Trabajador", utilidades.Utilidades.clonaLista(empleadosXML));
		} catch (Exception exc) {
			System.err.println("Ha habido algun error al generar el XML " + Constantes.FICHERO_XML_ERRORES);
			exc.printStackTrace();
			return;
		}
		
		
		
		try {
			Exportar.getInstancia().exportarXML(2, Constantes.FICHERO_XML_CUENTAS_ERRONEAS, "Trabajadores", "Trabajador", utilidades.Utilidades.clonaLista(empleadosErroresCuentas));
		} catch (Exception exc) {
			System.err.println("Ha habido algun error al generar el XML " + Constantes.FICHERO_XML_CUENTAS_ERRONEAS);
			exc.printStackTrace();
			return;
		}
		
		
		try {
			Exportar.getInstancia().exportarEXCEL(exportarEXCEL);
		} catch (IOException exc) {
			System.err.println("Ha habido algun error al generar el EXCEL " + Constantes.FICHERO_EXCEL_NOMBRE);
			exc.printStackTrace();
			return;
		}
	}
	
	public void generaNominas(Fecha fecha) {
		try {
			Importar.getInstancia().leeHoja2Excel(listaCuotas, listaRetenciones, listaSalariosCategoria, listaTrienios);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Ha habido algun error al leer la hoja 2 del excel");
			e.printStackTrace();
		}

		//Ya se han leido todos los datos de la hoja 2 del excel. Ahora se deben usar estos datos para obtener la nomina del empleado
		listaEmpleadosParaNominas = Utilidades.clonaLista(empleados);
		Utilidades.eliminaEmpleadosIncorrectos(listaEmpleadosParaNominas, empleadosXML);
		// Limpiamos los empleados que no estaban trabajando en aquella �poca porque no hab�an entrado
		for (int i = 0; i < listaEmpleadosParaNominas.size(); i++) {
			//System.out.println(fechaDeCalculo.toString() + " vs " + listaEmpleadosParaNominas.get(i).getFechaEntrada().toString());
			if (Fecha.esAnteriorA(fecha, listaEmpleadosParaNominas.get(i).getFechaEntrada())) {
				//System.out.println("Se elimina:"+listaEmpleadosParaNominas.get(i).getFechaEntrada().toString());
				listaEmpleadosParaNominas.remove(i);
				i--;
				//System.out.println("Eliminando... Ahora hay " + listaEmpleadosParaNominas.size() + " empleados.");
			}else {
				//System.out.println("__No se elimina:"+listaEmpleadosParaNominas.get(i).getFechaEntrada().toString());
			}
		}
		Utilidades.calculaTrienio(listaEmpleadosParaNominas, fecha);
		Utilidades.setEmpleadosSalarioBase(listaEmpleadosParaNominas, listaSalariosCategoria);
		Utilidades.formateaProrrateo(listaEmpleadosParaNominas);

        calcularNominas.calcularNominas(listaEmpleadosParaNominas, fecha, listaRetenciones, listaCuotas, listaNominas);
	}
	
	public ArrayList<Cuota> getListaCuotas() {
		return listaCuotas;
	}

	public ArrayList<Retencion> getListaRetenciones() {
		return listaRetenciones;
	}

	public ArrayList<SalarioCategoria> getListaSalariosCategoria() {
		return listaSalariosCategoria;
	}

	public ArrayList<Trienio> getListaTrienios() {
		return listaTrienios;
	}
	
}
