package principal;

import java.io.IOException;
import java.util.ArrayList;

import entradasalida.Importar;
import interfaz.UIPrincipal;
import objetos.*;

public class MainPruebasDani {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UIPrincipal uiprincipal = new UIPrincipal();
		ArrayList<Cuota> listaCuotas = new ArrayList<Cuota>();
		ArrayList<Retencion> listaRetenciones = new ArrayList<Retencion>();
		ArrayList<SalarioCategoria> listaSalariosCategoria = new ArrayList<SalarioCategoria>();
		ArrayList<Trienio> listaTrienios = new ArrayList<Trienio>();
		try {
			Importar.getInstancia().leeHoja2Excel(listaCuotas, listaRetenciones, listaSalariosCategoria, listaTrienios);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < listaCuotas.size(); i++)
			System.out.println(listaCuotas.get(i).getNombreCuota());
		for (int i = 0; i < listaRetenciones.size(); i++)
			System.out.println(listaRetenciones.get(i).getPorcentajeRetencion());
		for (int i = 0; i < listaSalariosCategoria.size(); i++)
			System.out.println(listaSalariosCategoria.get(i).getCategoria());
		for (int i = 0; i < listaTrienios.size(); i++)
			System.out.println(listaTrienios.get(i).getNumTrienio());
		
		System.out.println(Fecha.esAnteriorA(new Fecha("12/02/2018"), new Fecha("13/02/2018")));
		System.out.println(Fecha.esAnteriorA(new Fecha("13/02/2018"), new Fecha("12/02/2018")));
		System.out.println(Fecha.esIgualA(new Fecha("12/02/2018"), new Fecha("12/02/2018")));
		System.out.println(Fecha.esIgualA(new Fecha("12/02/2018"), new Fecha("13/02/2018")));
		System.out.println(Fecha.esPosteriorA(new Fecha("13/02/2018"), new Fecha("12/02/2018")));
		System.out.println(Fecha.esPosteriorA(new Fecha("12/02/2018"), new Fecha("13/01/2018")));
	}

}
