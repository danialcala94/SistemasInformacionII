package entradasalida;

import java.util.ArrayList;
import java.util.Comparator;

import javax.swing.SwingWorker;
/* Para XML */
import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.w3c.dom.*;

/* Para PDF */
import com.itextpdf.text.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.draw.*;
import com.itextpdf.text.pdf.events.PdfPageEventForwarder;

import bbdd.Categorias;
import bbdd.Empresas;
import bbdd.Trabajadorbbdd;

import java.io.*;
import java.text.DecimalFormat;

import objetos.Empleado;
import objetos.Fecha;
import objetos.Nomina;
import objetos.empleado.AtributoEmpleado;
import principal.HibernateUtils;
import utilidades.ComprobarError;
import utilidades.Constantes;
import utilidades.Utilidades;
import utilidades.nominasDefensa;

/**
 * (SINGLETON) Clase encargada de exportar informaci�n desde la aplicaci�n Java.
 * @author GAMING
 *
 */
public class Exportar {
	
	private static Exportar instancia = new Exportar();
	
	public Exportar() {
		// Singleton
	}
	
	/**
	 * Devuelve la instancia (singleton) de la clase.
	 * @return
	 */
	public static Exportar getInstancia() {
		return instancia;
	}
	
	/**
	 * Devuelve true si la clase est� instanciada, o false si no.
	 * @return
	 */
	public boolean estaInstanciado() {
		if (instancia != null)
			return true;
		return false;
	}
	
	/**
	 * Recibir� un <strong>String nombreArchivo</strong> que ser� el nombre final del archivo, y un <strong>String nombreRaiz</strong> que ser� el elemento ra�z.
	 * Adem�s, debe recibir un <strong>String nombreElemento</strong> que ser� el nombre que llevar� cada elemento (p. ej.: Trabajador).
	 * Debe recibir un ArrayList. El ArrayList contar�, para cada posici�n, con un empleado que contendr� pares <strong>claves</strong> - <strong>valores</strong> (p. ej.: Nombre - Federico).
	 * @param nombreArchivo
	 * @param llaves
	 * @param valores
	 * @throws ParserConfigurationException 
	 * @throws TransformerFactoryConfigurationError 
	 * @throws TransformerException 
	 * @throws Exception
	 */
	public void exportarXML(int numPractica, String nombreArchivo, String nombreRaiz, String nombreElemento, ArrayList<Empleado> empleados) throws ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException {
		// El ArrayList<Empleado> empleados es formateado autom�ticamente antes de ser exportado
		switch (numPractica) {
		case 1:
			empleados = Utilidades.darFormatoXML(empleados);
			break;

		case 2:
			empleados = Utilidades.darFormatoXML_P2(empleados);
			break;
		}
		
		
		// Constructores y varios para generar el XML
        DocumentBuilderFactory factoria = DocumentBuilderFactory.newInstance();
        DocumentBuilder constructor = factoria.newDocumentBuilder();
        DOMImplementation implementacion = constructor.getDOMImplementation();
        org.w3c.dom.Document documentoXML = implementacion.createDocument(null, nombreRaiz, null);
        documentoXML.setXmlVersion("1.0");

        // Creamos el nodo ra�z
        org.w3c.dom.Element raiz = documentoXML.getDocumentElement();
        //Por cada key creamos un item que contendr� la key y el value
        for (int i = 0; i < empleados.size(); i++) {
        	// Creamos un elemento con un id como atributo
        	org.w3c.dom.Element elemento = documentoXML.createElement(nombreElemento); 
        	String numFila = Integer.toString(empleados.get(i).getNumeroFila());
            elemento.setAttribute("id", numFila);
            
            org.w3c.dom.Element clave = null;
            // Cogemos, en bucle, todos los atributos
            for (int j = 0; j < empleados.get(i).getDatosEmpleado().size(); j++) {
            	// A�adimos el campo
            	clave = documentoXML.createElement(empleados.get(i).getDatosEmpleado().get(j).getNombreAtributo().toString());
            	// A�adimos el valor
            	Text valor = documentoXML.createTextNode(empleados.get(i).getDatosEmpleado().get(j).getValorAtributo().toString());
            	// Guardamos
            	clave.appendChild(valor);
            	elemento.appendChild(clave);
            }
            
            // A�adimos el elemento raiz
            raiz.appendChild(elemento);
        }                

        // Generamos el archivo en base al documento creado.
        Source fuente = new DOMSource(documentoXML);
        // Nos aseguramos de que tenga la extensi�n XML
        if (!nombreArchivo.endsWith(".xml"))
        	nombreArchivo += ".xml";
        StreamResult archivoSalida = new StreamResult(new java.io.File("resources/" + nombreArchivo));
        Transformer transformador = TransformerFactory.newInstance().newTransformer();
        transformador.setOutputProperty(OutputKeys.INDENT, "yes");
        transformador.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        transformador.transform(fuente, archivoSalida);
        
        System.out.println("Se ha generado el documento " + nombreArchivo + " correctamente.");
    }
	
	/**
	 * Exporta al EXCEL por defecto toda la informaci�n contenida en el <strong>ArrayList<Empleado> empleados</strong>.
	 * @param empleados
	 * @throws IOException
	 */
	public void exportarEXCEL(ArrayList<Empleado> empleados) throws IOException {
		//TODO
		//OJO
		//De momento solo modifica la linea del nif de los que se han corregido
		
		FileInputStream input = new FileInputStream(new File(Constantes.FICHERO_EXCEL_RUTA));
		XSSFWorkbook libro = new XSSFWorkbook(input);
		XSSFSheet hoja1 = libro.getSheetAt(0);
		
		Row fila0 = hoja1.getRow(0);
		int fin = fila0.getLastCellNum();
		int anchoPagina = fila0.getLastCellNum();
		Cell celdaFila0;
		Row fila;
		Cell celdaAux;
		for (int i = 0; i <empleados.size(); i++) {
			Empleado empleadoAux = empleados.get(i);
			//System.out.println("__________________________________");
			//System.out.println("Empleado de la fila:"+empleadoAux.getNumeroFila());
			//System.out.println("DNI: "+empleadoAux.getNif()+" Fila: "+empleadoAux.getNumeroFila());
			fila = hoja1.getRow(empleadoAux.getNumeroFila());
			//Seteamos el NIF Corregido
			celdaAux = fila.getCell(0);
			if(celdaAux==null) {
				celdaAux = fila.createCell(0);
			}
			celdaAux.setCellType(org.apache.poi.ss.usermodel.CellType.STRING);
			if(empleadoAux.getNif()=="NIF Vacio") {
				celdaAux.setCellValue("");
			}else {
				celdaAux.setCellValue(empleadoAux.getNif());
			}
			
			String email = empleadoAux.getEmail();
			String codigoCuenta = empleadoAux.getCuentaBancaria();
			int cont = 0;
			for (int k = 1; k < fin; k++) {
				celdaFila0 = fila0.getCell(k);
				celdaAux = fila.getCell(k);
				String value = celdaFila0.getStringCellValue();
				if (value.equalsIgnoreCase("Email")) {
					if (celdaAux == null)
						celdaAux = fila.createCell(k);
					celdaAux.setCellType(org.apache.poi.ss.usermodel.CellType.STRING);
					celdaAux.setCellValue(email);
					//System.out.println("He escrito " + email);
					cont++;
				} else if(value.equalsIgnoreCase("CodigoCuenta")) {
					if (celdaAux == null)
						celdaAux = fila.createCell(k);
					celdaAux.setCellType(org.apache.poi.ss.usermodel.CellType.STRING);
					celdaAux.setCellValue(codigoCuenta);
					//System.out.println("He escrito " + codigoCuenta);
					cont++;
				}
				if (cont == 2)
					break;
			}
			
			ArrayList<AtributoEmpleado> datosEmpleadoAux = empleadoAux.getDatosEmpleado();
			AtributoEmpleado atributo;
			for(int j=0; j<datosEmpleadoAux.size();j++) {
				atributo = datosEmpleadoAux.get(j);
				String nombreAtributo = atributo.getNombreAtributo();
				Object valorAtributo = atributo.getValorAtributo();
				for(int k=1; k<fin;k++) {
					celdaFila0 = fila0.getCell(k);
					celdaAux = fila.getCell(k);
					String value = celdaFila0.getStringCellValue();
					if(value.equalsIgnoreCase(nombreAtributo)) {
						if(celdaAux==null) {
							celdaAux = fila.createCell(k);
						}
						celdaAux.setCellType(org.apache.poi.ss.usermodel.CellType.STRING);
						if(!valorAtributo.getClass().getName().equalsIgnoreCase("java.util.Date")) {
							//System.out.println("Guardando "+nombreAtributo+ "--"+valorAtributo+" en la fila"+k);
							celdaAux.setCellValue((String) valorAtributo);
							break;
						}else {
							//TODO
							//Habr� que revisar como se guardan las fechas cuando sea necesario
							celdaAux.setCellValue("");
						}
					}
				}
			}
		}
		FileOutputStream fileOut = new FileOutputStream(new File(Constantes.FICHERO_EXCEL_RUTA));
		libro.write(fileOut);
		// Tambi�n habr� que cerrar el input (el File)
		input.close();
		libro.close();
		fileOut.close();
	}
	
	
	public static void generarPDF(ArrayList<Nomina> listaNominas) {
		for(int i=0; i<listaNominas.size(); i++) {
			generarPDF(listaNominas.get(i));
		}
	}
	
    /**
     * M�todo que genera un PDF concreto (tabla) a partir de la versi�n iText-5.0.5. Recibe un <strong>ArrayList<Empleado> empleados</strong> del que exporta todos sus atributos.
     * @param pdfAGenerar
     */
	
    public static void generarPDF(Nomina nomina) {
    	DecimalFormat df = new DecimalFormat("#0.00");

    	Empleado empleado = nomina.empleado;
    	Fecha fechaNomina = nomina.fecha;
        try {
            Document documento = new Document();
            try {
            	String nombreArchivo = Utilidades.getNombreFicheroNomina(empleado, fechaNomina);
            	
            	if (!nomina.esExtra)
            		PdfWriter.getInstance(documento, new FileOutputStream(new File("resources/" + nombreArchivo + ".pdf")));
            	else
            		PdfWriter.getInstance(documento, new FileOutputStream(new File("resources/" + nombreArchivo + "_extra.pdf")));
            	
            } catch (FileNotFoundException fnfExc) {
                System.out.println("[ERROR] No se puede acceder al fichero: " + fnfExc);
            }
            documento.open();
           
            Paragraph empty = new Paragraph("");
            
            //Tabla de datos de la empresa
            PdfPTable tablaDatosEmpresa = new PdfPTable(1);
            Paragraph nombreEmpresa = new Paragraph(empleado.getEmpresa());
            Paragraph cifEmpresa = new Paragraph(empleado.getCifEmpresa());
            Paragraph dirEmpresa = new Paragraph("<Direccion de la empresa>");
            Paragraph telefonoEmpresa = new Paragraph("<Telefono de la empresa>");
            PdfPCell celdaEmpresa = new PdfPCell();
            celdaEmpresa.addElement(nombreEmpresa);
            celdaEmpresa.addElement(cifEmpresa);
            celdaEmpresa.addElement(dirEmpresa);
            celdaEmpresa.addElement(telefonoEmpresa);
            tablaDatosEmpresa.addCell(celdaEmpresa);
            documento.add(tablaDatosEmpresa);
            
            //Tabla de datos del empleado
            PdfPTable tablaDatosEmpleado = new PdfPTable(1);
            Paragraph textoEmpleado = new Paragraph("Empleado:");
            String nombreCompleto = empleado.getNombre() + " " +empleado.getApellido1();
            if(empleado.getApellido2() != null) {
            	nombreCompleto += " "+empleado.getApellido2();
            }
            PdfPCell celdaEmpleado = new PdfPCell();
            celdaEmpleado.addElement(new Paragraph(nombreCompleto));
            celdaEmpleado.addElement(new Paragraph("DNI: "+empleado.getNif()));
            celdaEmpleado.addElement(new Paragraph("<Direccion del empleado>"));
            celdaEmpleado.addElement(new Paragraph("<Telefono del empleado>"));
            celdaEmpleado.addElement(new Paragraph("IBAN: " +empleado.getIBAN()));
            celdaEmpleado.addElement(new Paragraph("Categoria: "+ empleado.getCategoria()));
            celdaEmpleado.addElement(new Paragraph("Bruto anual: "+ nomina.brutoAnual));
            celdaEmpleado.addElement(new Paragraph("Fecha de alta: " + empleado.getFechaEntrada().toString()));
            tablaDatosEmpleado.addCell(celdaEmpleado);
            documento.add(tablaDatosEmpleado);
            
            //Tabla de la fecha de la nomina
            PdfPTable tablaFecha = new PdfPTable(1);
            String textoNomina = "Nomina: "+Utilidades.mesToString(fechaNomina.getMes()) +" "+ fechaNomina.getAnno();
            if(nomina.esExtra) {
            	textoNomina += " [Extra]";
            }
            Paragraph parrafoFecha = new Paragraph(textoNomina);
            PdfPCell celdaFecha = new PdfPCell();
            celdaFecha.addElement(parrafoFecha);
            tablaFecha.addCell(celdaFecha);
            documento.add(tablaFecha);
            
            
            PdfPTable tablaNomina = new PdfPTable(5);
            //Encabezado de la tabla
            tablaNomina.addCell(empty);
            tablaNomina.addCell("Cant.");
            tablaNomina.addCell("Imp. Unit.");
            tablaNomina.addCell("Dev.");
            tablaNomina.addCell("Deducc.");
            //Salario base
            tablaNomina.addCell("Salario Base");
            tablaNomina.addCell("30 Dias");
            tablaNomina.addCell(df.format(nomina.salarioBase/30));
            tablaNomina.addCell(df.format(nomina.salarioBase));
            tablaNomina.addCell(empty);
            //Prorrata
            tablaNomina.addCell("Prorrata");
            tablaNomina.addCell("30 Dias");
            tablaNomina.addCell(df.format(nomina.prorrata/30));
            tablaNomina.addCell(df.format(nomina.prorrata));
            tablaNomina.addCell(empty);
            //Complemento
            tablaNomina.addCell("Complemento");
            tablaNomina.addCell("30 Dias");
            tablaNomina.addCell(df.format(nomina.complementos/30));
            tablaNomina.addCell(df.format(nomina.complementos));
            tablaNomina.addCell(empty);
            //Antiguedad
            tablaNomina.addCell("Antig�edad");
            tablaNomina.addCell(df.format(nomina.numTrienios)+ " Trienios");
            tablaNomina.addCell(df.format(nomina.antiguedad/30));
            tablaNomina.addCell(df.format(nomina.antiguedad));
            tablaNomina.addCell(empty);
            //Contingencias
            //Generales
            tablaNomina.addCell("Contingencias Generales");
            tablaNomina.addCell(Double.toString(nomina.porcentajeContinGen)+" %");
            tablaNomina.addCell("de "+df.format(nomina.baseDescuentos));
            tablaNomina.addCell(empty);
            tablaNomina.addCell(df.format(nomina.continGen));
            //Desempleo
            tablaNomina.addCell("Desempleo");
            tablaNomina.addCell(Double.toString(nomina.porcentajeDesempleo)+" %");
            tablaNomina.addCell("de "+df.format(nomina.baseDescuentos));
            tablaNomina.addCell(empty);
            tablaNomina.addCell(df.format(nomina.desempleo));
            //Cuota Formacion
            tablaNomina.addCell("Couta Formaci�n");
            tablaNomina.addCell(Double.toString(nomina.porcentajeFormacion)+" %");
            tablaNomina.addCell("de "+df.format(nomina.baseDescuentos));
            tablaNomina.addCell(empty);
            tablaNomina.addCell(df.format(nomina.formacion));
            //IRPF
            tablaNomina.addCell("IRPF");
            tablaNomina.addCell(Double.toString(nomina.porcentajeIRPF)+" %");
            tablaNomina.addCell("de "+df.format(nomina.totalDevengos));
            tablaNomina.addCell(empty);
            tablaNomina.addCell(df.format(nomina.irpf));
            //Resumen
            tablaNomina.addCell("Total Devengos");
            tablaNomina.addCell(empty);
            tablaNomina.addCell(empty);
            tablaNomina.addCell(df.format(nomina.totalDevengos));
            tablaNomina.addCell(empty);
            tablaNomina.addCell("Total Deducciones");
            tablaNomina.addCell(empty);
            tablaNomina.addCell(empty);
            tablaNomina.addCell(empty);
            tablaNomina.addCell(df.format(nomina.totalDeducciones));
            
            documento.add(tablaNomina);
            
            PdfPTable tablaLiquido = new PdfPTable(2);
            tablaLiquido.addCell("Liquido a percibir: ");
            tablaLiquido.addCell(df.format(nomina.liquidoAPercibir));
            
            documento.add(tablaLiquido);

            PdfPTable tablaCosteEmpresa = new PdfPTable(3);
            //Encabezado
            tablaCosteEmpresa.addCell(empty);
            tablaCosteEmpresa.addCell("%");
            tablaCosteEmpresa.addCell("Importe");
            
            tablaCosteEmpresa.addCell("Calculo empresario: BASE");
            tablaCosteEmpresa.addCell(empty);
            tablaCosteEmpresa.addCell(df.format(nomina.baseDescuentos));
            
            tablaCosteEmpresa.addCell("Contingencias comunes");
            tablaCosteEmpresa.addCell(Double.toString(nomina.porcentajeEmpresaContinGen)+" %");
            tablaCosteEmpresa.addCell(df.format(nomina.empresaContinGen));
            
            tablaCosteEmpresa.addCell("Desempleo");
            tablaCosteEmpresa.addCell(Double.toString(nomina.porcentajeEmpresaDesempleo)+" %");
            tablaCosteEmpresa.addCell(df.format(nomina.empresaDesempleo));
            
            tablaCosteEmpresa.addCell("Formacion");
            tablaCosteEmpresa.addCell(Double.toString(nomina.porcentajeEmpresaFormacion)+" %");
            tablaCosteEmpresa.addCell(df.format(nomina.empresaFormacion));
            
            tablaCosteEmpresa.addCell("Accidentes de trabajo");
            tablaCosteEmpresa.addCell(Double.toString(nomina.porcentajeEmpresaAccidentes)+" %");
            tablaCosteEmpresa.addCell(df.format(nomina.empresaAccidentes));
            
            tablaCosteEmpresa.addCell("FOGASA");
            tablaCosteEmpresa.addCell(Double.toString(nomina.porcentajeEmpresaFOGASA)+" %");
            tablaCosteEmpresa.addCell(df.format(nomina.empresaFOGASA));
            
            tablaCosteEmpresa.addCell("Total empresario");
            tablaCosteEmpresa.addCell(empty);
            tablaCosteEmpresa.addCell(df.format(nomina.totalEmpresario));
            tablaCosteEmpresa.addCell("Coste total trabajador");
            tablaCosteEmpresa.addCell(empty);
            tablaCosteEmpresa.addCell(df.format(nomina.costeTrabajador));
            
            documento.add(tablaCosteEmpresa);
            
            documento.close();
            
            if(nomina.esExtra) {
            	System.out.println("\t�Se ha exportado a PDF la nomina extra del empleado con DNI: "+empleado.getNif());
            }else {
            	System.out.println("\t�Se ha exportado a PDF la nomina del empleado con DNI: "+empleado.getNif());
            }
            
        } catch (DocumentException docExc) {
            System.out.println("[ERROR] No se ha podido generar correctamente la nomina: " + docExc);
        }
	}  
    
    
    
    public static void generarPDFDefensa(bbdd.Nomina nominaC, bbdd.Nomina nominaGlobal) {
    	DecimalFormat df = new DecimalFormat("#0.00");
        try {
            Document documento = new Document();
            try {
        		PdfWriter.getInstance(documento, new FileOutputStream(new File("resources/Defensa2018_Alcal�_Bay�n_Crespo.pdf")));
            } catch (FileNotFoundException fnfExc) {
                System.out.println("[ERROR] No se puede acceder al fichero: " + fnfExc);
            }
            documento.open();
           
            Paragraph empty = new Paragraph("");
            
            //Informacion de los integrantes del grupo
            PdfPTable tablaIntegrantes = new PdfPTable(1);
            tablaIntegrantes.addCell("Nombre de los integrantes del grupo:");
            tablaIntegrantes.addCell("\tAlcal� Valera, Daniel\t45121766-K");
            tablaIntegrantes.addCell("\tBay�n Guti�rrez, Mart�n\t71466464-J");
            tablaIntegrantes.addCell("\tCrespo Torbado, Beatriz\t71471107-X");
            tablaIntegrantes.addCell("\n");
            documento.add(tablaIntegrantes);         
            
            PdfPTable tablaNominaC = new PdfPTable(1);
            tablaNominaC.addCell("N�mina m�nima del fichero c");
            bbdd.Trabajadorbbdd trabajadorC = nominasDefensa.trabajadorFromID(nominaC.getIdTrabajador());
            bbdd.Categorias categoriaC = nominasDefensa.categoriaFromID(trabajadorC.getIdCategoria());
            String infoC = "\t"+ trabajadorC.getNombre()+" "+trabajadorC.getApellido1()+" ";
            if(trabajadorC.getApellido2() != null && trabajadorC.getApellido2() != "") {
            	infoC+=trabajadorC.getApellido2()+" ";
            }
            infoC+="\t"+categoriaC.getNombreCategoria()+"\t"+df.format(nominaC.getLiquidoNomina())+"�";
            tablaNominaC.addCell(infoC);
            tablaNominaC.addCell(empty);
            documento.add(tablaNominaC);     
            
            PdfPTable tablaNominaBD = new PdfPTable(1);
            tablaNominaBD.addCell("N�mina m�nima de la base de datos");
            bbdd.Trabajadorbbdd trabajadorBD = nominasDefensa.trabajadorFromID(nominaGlobal.getIdTrabajador());
            bbdd.Categorias categoriaBD = nominasDefensa.categoriaFromID(trabajadorBD.getIdCategoria());
            String infoBD = "\t"+trabajadorBD.getNombre()+" "+trabajadorBD.getApellido1()+" ";
            if(trabajadorBD.getApellido2() != null && trabajadorBD.getApellido2() != "") {
            	infoBD+=trabajadorBD.getApellido2()+" ";
            }
            infoBD+="\t"+categoriaBD.getNombreCategoria()+"\t"+df.format(nominaGlobal.getLiquidoNomina())+"�";
            tablaNominaBD.addCell(infoBD);
            tablaNominaBD.addCell(empty);
            documento.add(tablaNominaBD);

            documento.close();      
        } catch (DocumentException docExc) {
            System.out.println("[ERROR] No se ha podido generar correctamente la nomina: " + docExc);
        }
	}  
}
