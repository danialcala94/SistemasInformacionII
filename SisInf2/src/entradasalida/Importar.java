package entradasalida;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.collections4.Trie;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import objetos.Cuota;
import objetos.Empleado;
import objetos.Fecha;
import objetos.Retencion;
import objetos.SalarioCategoria;
import objetos.Trienio;
import utilidades.Constantes;
import utilidades.Utilidades;

/**
 * (SINGLETON) Clase encargada de introducir informaci�n a la aplicaci�n Java.
 * @author GAMING
 *
 */
public class Importar {
	
	private static final Importar instancia = new Importar();
	
	public Importar() {
		// Singleton
	}
	
	/**
	 * Devuelve la instancia (singleton) de la clase.
	 * @return
	 */
	public static Importar getInstancia() {
		return instancia;
	}
	
	/**
	 * Devuelve true si la clase est� instanciada, o false si no.
	 * @return
	 */
	public boolean estaInstanciado() {
		if (instancia != null)
			return true;
		return false;
	}
	
	
	/*	EXCEL	*/
	/************/
	
	/**
	 * Lee la hoja (1) del excel y almacena los valores le�dos en el <strong>ArrayList<Empleado> empleados</strong> pasado como par�metro.
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public void leeHoja1Excel(ArrayList<Empleado> empleados) throws IOException {
		FileInputStream archivo = new FileInputStream(new File(Constantes.FICHERO_EXCEL_RUTA));
		XSSFWorkbook libro = new XSSFWorkbook(archivo);
		
		XSSFSheet hoja1 = libro.getSheetAt(0);	// Obtenemos la hoja1 del Libro
		int numFilas = hoja1.getLastRowNum() + 1;
		int numColumnas = hoja1.getRow(0).getLastCellNum();
		//System.out.println("Numero de filas: " + numFilas);
		//System.out.println("Numero de columnas: " + numColumnas);
		

		Row fila0 = hoja1.getRow(0);
		Cell celdaFila0;
		
		for (int i = 1;i < numFilas; i++) {
			Row filaAux = hoja1.getRow(i);
			Empleado empleadoAux = null;
			for (int j = 0; j < numColumnas; j++) {
				Cell celdaAux = filaAux.getCell(j);
				if (j == 0) {
					if(celdaAux != null) {
						if (celdaAux.getStringCellValue() == "" || Utilidades.soloEspacios(celdaAux.getStringCellValue())) {
							empleadoAux = new Empleado("NIF Vacio", i);
						} else {
							empleadoAux = new Empleado(celdaAux.getStringCellValue(), i);
						}
					}
				} else if (celdaAux != null) {
					if (empleadoAux == null) {
						empleadoAux = new Empleado("NIF Vacio", i);
					}
					celdaFila0 = fila0.getCell(j);
					switch(celdaAux.getCellType()) {
						case Cell.CELL_TYPE_NUMERIC:
							//Estamos en el caso de que se lee la fecha de entrada del empleado
							String fechaDiv[] = celdaAux.getDateCellValue().toString().split(" ");
							Fecha fechaEntrada = new Fecha(fechaDiv[2], fechaDiv[1], fechaDiv[5]);
							empleadoAux.addAtributo(celdaFila0.getStringCellValue(), fechaEntrada);
							break;
						case Cell.CELL_TYPE_STRING:
							empleadoAux.addAtributo(celdaFila0.getStringCellValue(), celdaAux.getStringCellValue());
							break;
					}

				}
			}
			if (empleadoAux != null) {
				//Si el empleado no se lleg� a crear (no hab�a dni en esa fila) no se almacena el empleado 
				empleados.add(empleadoAux);
			}
		}
		System.out.println("Se ha obtenido la informaci�n de " + empleados.size() + " empleados de la hoja EXCEL");
		// Cerramos el libro excel
		libro.close();
		archivo.close();
	}
	
	
	public void leeHoja2Excel(ArrayList<Cuota> listaCuotas, ArrayList<Retencion> listaRetencion, ArrayList<SalarioCategoria> listaSalariosCategoria, ArrayList<Trienio> listaTrienios) throws IOException {
		FileInputStream archivo = new FileInputStream(new File(Constantes.FICHERO_EXCEL_RUTA));
		XSSFWorkbook libro = new XSSFWorkbook(archivo);
		
		XSSFSheet hoja2 = libro.getSheetAt(1);	// Obtenemos la hoja2 del Libro
		
		
		Row filaActual;
		Cell celdaActual;
		
		for(int i=1; i<15; i++) {
			filaActual = hoja2.getRow(i);
			SalarioCategoria salarioAct = new SalarioCategoria(
					filaActual.getCell(0).getStringCellValue(),
					filaActual.getCell(1).getNumericCellValue(),
					filaActual.getCell(2).getNumericCellValue(),
					filaActual.getCell(3).getNumericCellValue()
					);
			listaSalariosCategoria.add(salarioAct);
		}
		
		for(int i=17; i<25; i++) {
			filaActual = hoja2.getRow(i);
			Cuota cuotaAct = new Cuota(
					filaActual.getCell(0).getStringCellValue(),
					filaActual.getCell(1).getNumericCellValue()
					);
			listaCuotas.add(cuotaAct);
		}
		
		for(int i=18; i<36; i++) {
			filaActual = hoja2.getRow(i);
			Trienio trienioAct = new Trienio(
					filaActual.getCell(3).getNumericCellValue(),
					filaActual.getCell(4).getNumericCellValue()
					);
			listaTrienios.add(trienioAct);
		}
		
		for(int i=1; i<50; i++) {
			filaActual = hoja2.getRow(i);
			Retencion retencionAct = new Retencion(
					filaActual.getCell(5).getNumericCellValue(),
					filaActual.getCell(6).getNumericCellValue()
					);
			listaRetencion.add(retencionAct);
		}
		
		libro.close();
		archivo.close();
	}
}
