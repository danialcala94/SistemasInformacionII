package entradasalida;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.persistence.Id;

import org.apache.poi.ddf.EscherColorRef.SysIndexProcedure;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbdd.Categorias;
import bbdd.Empresas;
import bbdd.Nomina;
import bbdd.Trabajadorbbdd;
import objetos.Empleado;
import principal.HibernateUtils;

/*
 * Clase para los metodos encargados de exportar los datos a la BD
 */
public class exportacionBD {
	public static void exportarCategorias(ArrayList<Categorias> listaCategorias) {
		//Para almacenar en BD las categorias primero tenemos que obtener las categorias almacenadas en la BD y comprobar si son las mismas
		ArrayList<Categorias> listaCatBD = getCategoriasBD();
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
		
		int nuevas = 0;
		int actualizadas = 0;

		for(int i=0; i<listaCategorias.size(); i++) {
			int IdBD = idCategoriaBD(listaCategorias.get(i), listaCatBD);
			if(IdBD == -1) {
				//Guarda la categoria como una nueva
				session.save(listaCategorias.get(i));
				nuevas++;
			}else {
				//Almacena el ID de la Base de datos en la categoria que vamos a insertar y hace un update
				listaCategorias.get(i).setIdCategoria(IdBD);
				session.update(listaCategorias.get(i));
				actualizadas++;
			}
		}
		tx.commit();
		session.close();
		if(nuevas>0) {
			System.out.println("Se han insertado en la base de datos "+nuevas+" categorias nuevas");
		}
		if(actualizadas>0) {
			System.out.println("Se han actualizado/comprobado los datos de "+actualizadas+" categorias");
		}
	}
	
	public static void exportarEmpresas(ArrayList<Empresas> listaEmpresas) {
		//Para almacenar en BD las Empresas primero tenemos que obtener las empresas almacenadas en la BD y comprobar si son las mismas
		ArrayList<Empresas> listaEmpresasBD = getEmpresasBD();
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
		
		int nuevas = 0;
		int actualizadas = 0;

		for(int i=0; i<listaEmpresas.size(); i++) {
			int IdBD = idEmpresaBD(listaEmpresas.get(i), listaEmpresasBD);
			if(IdBD == -1) {
				//Guarda la categoria como una nueva
				session.save(listaEmpresas.get(i));
				nuevas++;
			}else {
				//Almacena el ID de la Base de datos en la categoria que vamos a insertar y hace un update
				listaEmpresas.get(i).setIdEmpresa(IdBD);
				session.update(listaEmpresas.get(i));
				actualizadas++;
			}
		}
		tx.commit();
		session.close();
		if(nuevas>0) {
			System.out.println("Se han insertado en la base de datos "+nuevas+" empresas nuevas");
		}
		if(actualizadas>0) {
			System.out.println("Se han actualizado/comprobado los datos de "+actualizadas+" empresas");
		}
	}
	
	public static void exportarTrabajadores(ArrayList<Trabajadorbbdd> listaTrabajadores) {
		getIDTrabajadores(listaTrabajadores);
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
		int nuevas = 0;
		int actualizadas = 0;
		for(int i=0; i<listaTrabajadores.size(); i++) {
			if(listaTrabajadores.get(i).getIdTrabajador() == -1) {
				//El trabajador no estaba en la base de datos, se inserta como nuevo
				session.save(listaTrabajadores.get(i));
				nuevas++;
			}else{
				//El trabajador ya estaba en la base de datos con id IdBD
				session.update(listaTrabajadores.get(i));
				actualizadas++;
			}
		}
		tx.commit();
		session.close();
		if(nuevas>0) {
			System.out.println("Se han insertado en la base de datos "+nuevas+" trabajadores nuevos");
		}
		if(actualizadas>0) {
			System.out.println("Se han actualizado/comprobado los datos de "+actualizadas+" trabajadores");
		}
	}
	
	
	public static void exportarNominas(ArrayList<Nomina> listaNominas) {
		getIDNomina(listaNominas);
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
		int nuevas = 0;
		int actualizadas = 0;
		for(int i=0; i<listaNominas.size(); i++) {
			if(listaNominas.get(i).getIdNomina() == -1) {
				//La nomina no estaba en la base de datos, se inserta como nueva
				session.save(listaNominas.get(i));
				nuevas++;
			}else {
				//La nomina ya estaba en la base de datos con id IdBD
				session.merge(listaNominas.get(i));
				actualizadas++;
			}
		}
		tx.commit();
		session.close();
		if(nuevas>0) {
			System.out.println("Se han insertado en la base de datos "+nuevas+" nominas nuevas");
		}
		if(actualizadas>0) {
			System.out.println("Se han actualizado/comprobado los datos de "+actualizadas+" nominas");
		}
	}
	

	public static ArrayList<Categorias> getCategoriasBD(){
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		ArrayList<Categorias> listaCatBD = (ArrayList<Categorias>) session.createQuery("from Categorias").list();
		session.close();
		return listaCatBD;
	}
	
	public static ArrayList<Empresas> getEmpresasBD(){
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		ArrayList<Empresas> listaEmpresasBD = (ArrayList<Empresas>) session.createQuery("from Empresas").list();
		session.close();
		return listaEmpresasBD;
	}
	
	
	
	
	public static int idCategoriaBD(Categorias categoria, ArrayList<Categorias> listaCategorias) {
		for(int i=0; i<listaCategorias.size(); i++) {
			if(categoria.getNombreCategoria().equalsIgnoreCase(listaCategorias.get(i).getNombreCategoria())) {
				//Si la categoria tiene el mismo nombre estaba ya en la base de datos
				return listaCategorias.get(i).getIdCategoria();
			}
		}
		//Si no se encuentra una categoria con ese nombre se inserta como nueva
		return -1;
	}
	
	public static int idEmpresaBD(Empresas empresa, ArrayList<Empresas> listaEmpresas) {
		for(int i=0; i<listaEmpresas.size(); i++) {
			if(empresa.getCif().equalsIgnoreCase(listaEmpresas.get(i).getCif())) {
				//Si la empresa tiene el mismo CIF que una que ya estaba en la BD es la misma 
				return listaEmpresas.get(i).getIdEmpresa();
			}
		}
		//Si no se encuentra una empresa con ese CIF se inserta como nueva
		return -1;
	}
	
	public static int idTrabajadorBD(Trabajadorbbdd trabajador) {
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Query qry = session.createQuery("from Trabajadorbbdd where NIFNIE like :nif AND Apellido1 like :ap1");
		qry.setString("nif",'%'+trabajador.getNifnie()+'%');
		qry.setString("ap1",'%'+trabajador.getApellido1()+'%');
		ArrayList<Trabajadorbbdd> listaTrabajadores= (ArrayList<Trabajadorbbdd>) qry.getResultList();
		session.close();
		if(listaTrabajadores.size() == 0) {
			return -1;
		}
		return listaTrabajadores.get(0).getIdTrabajador();
	}
	
	public static int idNominaBD(Nomina nomina) {
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Query qry = session.createQuery("from Nomina where Mes like :mes AND Anio like :anio AND IdTrabajador like :trabajador");
		qry.setInteger("mes",nomina.getMes());
		qry.setInteger("anio",nomina.getAnio());
		qry.setInteger("trabajador",nomina.getIdTrabajador());
		ArrayList<Nomina> listaNominas= (ArrayList<Nomina>) qry.list();
		session.close();
		if(listaNominas.size() == 0) {
			return -1;
		}
		return listaNominas.get(0).getIdNomina();
	}
	
	
	public static void getIDTrabajadores(ArrayList<Trabajadorbbdd> listaTrabajadores) {
		Trabajadorbbdd trabajadorAux;
		for(int i=0; i<listaTrabajadores.size(); i++) {
			trabajadorAux = listaTrabajadores.get(i);
			trabajadorAux.setIdTrabajador(idTrabajadorBD(trabajadorAux));
		}
	}
	
	//Setea los id de las nominas que estan en la BD
	private static void getIDNomina(ArrayList<Nomina> listaNominas) {
		Nomina nominaAux;
		for(int i=0; i<listaNominas.size(); i++) {
			nominaAux = listaNominas.get(i);
			nominaAux.setIdNomina(idNominaBD(nominaAux));
		}
	}
}