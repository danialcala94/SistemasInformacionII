package objetos;

public class Retencion {
	double bruto;
	double porcentajeRetencion;
	
	public Retencion(double d, double porcentajeRetencion) {
		this.bruto = d;
		this.porcentajeRetencion = porcentajeRetencion;
	}

	public double getBruto() {
		return bruto;
	}

	public void setBruto(double bruto) {
		this.bruto = bruto;
	}

	public double getPorcentajeRetencion() {
		return porcentajeRetencion;
	}

	public void setPorcentajeRetencion(double porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
}
