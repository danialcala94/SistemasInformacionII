package objetos;

import bbdd.Categorias;

public class SalarioCategoria {
	String categoria;
	double salarioBase;
	double complemento;
	double codigoCotizacion;
	
	private Categorias categoriabbdd;

	public SalarioCategoria(String categoria, double d, double e, double f) {
		this.categoria = categoria;
		this.salarioBase = d;
		this.complemento = e;
		this.codigoCotizacion = f;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public double getComplemento() {
		return complemento;
	}

	public void setComplemento(double complemento) {
		this.complemento = complemento;
	}

	public double getCodigoCotizacion() {
		return codigoCotizacion;
	}

	public void setCodigoCotizacion(double codigoCotizacion) {
		this.codigoCotizacion = codigoCotizacion;
	}

	public Categorias getCategoriabbdd() {
		return categoriabbdd;
	}

	public void setCategoriabbdd(Categorias categoriabbdd) {
		this.categoriabbdd = categoriabbdd;
	}
}
