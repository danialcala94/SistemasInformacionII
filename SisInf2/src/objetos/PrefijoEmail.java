package objetos;

public class PrefijoEmail {
	String nombre;
	int num;
	
	
	public PrefijoEmail(String prefijo) {
		this.nombre = prefijo;
		this.num = 1;
	}
	
	public void addEntrada() {
		this.num++;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getEntrada() {
		return Integer.toString(num);
	}
}
