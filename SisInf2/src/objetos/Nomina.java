package objetos;

import java.util.ArrayList;

public class Nomina {
	
	public Fecha fecha;
	public double brutoAnual;
	public Empleado empleado;
	
	public double salarioBase;
	public double prorrata;
	public double complementos;
	public int numTrienios;
	public double antiguedad;
	
	public ArrayList<Cuota> listaCoutas;
	
	public double totalDeducciones = 0;
	public double totalDevengos = 0;
	public double liquidoAPercibir = 0;
	
	public double baseDescuentos;
	
	public double porcentajeContinGen;
	public double continGen;
	public double porcentajeDesempleo;
	public double desempleo;
	public double porcentajeFormacion;
	public double formacion;
	public double porcentajeIRPF;
	public double irpf;
	
	public double empresaContinGen;
	public double empresaDesempleo;
	public double empresaFormacion;
	public double empresaAccidentes;
	public double empresaFOGASA;
	public double porcentajeEmpresaContinGen;
	public double porcentajeEmpresaDesempleo;
	public double porcentajeEmpresaFormacion;
	public double porcentajeEmpresaAccidentes;
	public double porcentajeEmpresaFOGASA;
	
	public double totalEmpresario;
	public double costeTrabajador;
	
	public boolean esExtra = false;
	
	private bbdd.Nomina nominabbdd;
	
	public Nomina(Fecha fecha, Empleado empleado, double brutoAnual, double salarioBase, double prorrata, double complementos, ArrayList<Cuota> listaCoutas, double porcentajeIRPF, double baseDescuentos) {
		this.fecha = fecha;
		this.empleado = empleado;
		this.brutoAnual = brutoAnual;
		this.salarioBase = salarioBase;
		this.prorrata = prorrata;
		this.complementos = complementos;
		this.listaCoutas = listaCoutas;
		this.porcentajeIRPF = porcentajeIRPF;
		this.baseDescuentos = baseDescuentos;
		calculosNomina();
	}
	
	/**
	 * Constructor espec�fico para n�minas extra.
	 * @param fecha
	 * @param empleado
	 * @param brutoAnual
	 * @param salarioBase
	 * @param prorrata
	 * @param complementos
	 * @param listaCoutas
	 * @param porcentajeIRPF
	 * @param baseDescuentos
	 * @param extra
	 */
	public Nomina(Fecha fecha, Empleado empleado, double brutoAnual, double salarioBase, double prorrata, double complementos, ArrayList<Cuota> listaCoutas, double porcentajeIRPF, double baseDescuentos, boolean extra) {
		this.fecha = fecha;
		this.empleado = empleado;
		this.brutoAnual = brutoAnual;
		this.salarioBase = salarioBase;
		this.prorrata = prorrata;
		this.complementos = complementos;
		this.listaCoutas = listaCoutas;
		this.porcentajeIRPF = porcentajeIRPF;
		this.baseDescuentos = baseDescuentos;
		this.esExtra = true;
		calculosNominaExtra();
	}
	
	private void calculaAntiguedad() {
		int mesCambio = empleado.getMesCambioTrienio();
		if(mesCambio == -1) {
			//Este a�o no toca cambio de trienio
			//El trienio es el anterior
			Trienio trienioAct = empleado.getTrienioAnterior();
			if(trienioAct != null) {
				numTrienios = (int) trienioAct.numTrienio;
				antiguedad = trienioAct.importeMensual;
			}else {
				antiguedad = 0;
			}
		}else {
			//Si hay cambio este a�o habra que ver en que mes y si es mes es este
			if(mesCambio <= this.fecha.getMes()) {
				//El mes de cambio es este
				if(empleado.getTrienioSiguiente() != null){
					Trienio trienioAct = empleado.getTrienioSiguiente();
					// AQU� PETA PORQUE PUEDE RECIBIR null EN trienioAct
					numTrienios = (int) trienioAct.numTrienio;
					//numTrienios = 2;
					antiguedad = trienioAct.importeMensual;
					//antiguedad = 25;
				}else{
					numTrienios = 0;
					antiguedad = 0;
				}
			}else {
				if(empleado.getTrienioAnterior() != null){
					Trienio trienioAct = empleado.getTrienioAnterior();
					// AQU� PETA PORQUE PUEDE RECIBIR null EN trienioAct
					numTrienios = (int) trienioAct.numTrienio;
					//numTrienios = 2;
					antiguedad = trienioAct.importeMensual;
					//antiguedad = 25;
				}else{
					numTrienios = 0;
					antiguedad = 0;
				}
			}
		}
	}
	
	/**
	 * M�todo exclusivo para crear las n�minas extra.
	 */
	private void calculosNominaExtra() {
		calculaAntiguedad();
		
		// Debemos saber el n�mero de meses implicados en la n�mina extra
		int mesesAplicables = 0;
		if (empleado.getFechaEntrada().getAnno() < fecha.getAnno())
			mesesAplicables = 6;
		else if (empleado.getFechaEntrada().getAnno() == fecha.getAnno()) {
			// DICIEMBRE
			if (fecha.getMes() == 12) {
				if (empleado.getFechaEntrada().getMes() <= 6)
					mesesAplicables = 6;
				else
					mesesAplicables = 12 - empleado.getFechaEntrada().getMes();
			} else if (fecha.getMes() == 6) {
				if (empleado.getFechaEntrada().getMes() >= 6)
					mesesAplicables = 0;
				else
					mesesAplicables = 6 - empleado.getFechaEntrada().getMes();
			}
		}
		
		totalDevengos = salarioBase + complementos + antiguedad;
		double totalNominaExtra = totalDevengos - (6 - mesesAplicables) * (totalDevengos / 6);
		if (totalNominaExtra > -1 && totalNominaExtra < 1)
			totalNominaExtra = 0;
		
		porcentajeContinGen = 0;
		porcentajeDesempleo = 0;
		porcentajeFormacion = 0;
		continGen = 0;
		desempleo = 0;
		formacion = 0;
		irpf = totalNominaExtra * porcentajeIRPF / 100;
		
		totalDeducciones = irpf;
		
		liquidoAPercibir = totalNominaExtra - totalDeducciones;
		
		porcentajeEmpresaContinGen = 0;
		porcentajeEmpresaDesempleo = 0;
		porcentajeEmpresaFormacion = 0;
		porcentajeEmpresaAccidentes = 0;
		porcentajeEmpresaFOGASA = 0;
		
		empresaContinGen = 0;
		empresaDesempleo = 0;
		empresaFormacion = 0;
		empresaAccidentes = 0;
		empresaFOGASA = 0;
		
		totalEmpresario = empresaContinGen + empresaDesempleo + empresaFormacion + empresaAccidentes + empresaFOGASA;
		
		costeTrabajador = totalNominaExtra + totalEmpresario;
	}
	
	private void calculosNomina(){
		calculaAntiguedad();
		
		totalDevengos = salarioBase + prorrata + complementos + antiguedad;
		
		porcentajeContinGen = listaCoutas.get(0).getPorcentajeCuota();
		porcentajeDesempleo = listaCoutas.get(1).getPorcentajeCuota();
		porcentajeFormacion = listaCoutas.get(2).getPorcentajeCuota();
		
		continGen = baseDescuentos*porcentajeContinGen/100;
		desempleo = baseDescuentos*porcentajeDesempleo/100;
		formacion = baseDescuentos*porcentajeFormacion/100;
		irpf = totalDevengos*porcentajeIRPF/100;
		
		totalDeducciones = continGen + desempleo + formacion + irpf;
	
		liquidoAPercibir = totalDevengos - totalDeducciones;
		
		porcentajeEmpresaContinGen = listaCoutas.get(3).getPorcentajeCuota();
		porcentajeEmpresaDesempleo = listaCoutas.get(5).getPorcentajeCuota();
		porcentajeEmpresaFormacion = listaCoutas.get(6).getPorcentajeCuota();
		porcentajeEmpresaAccidentes = listaCoutas.get(7).getPorcentajeCuota();
		porcentajeEmpresaFOGASA = listaCoutas.get(4).getPorcentajeCuota();
		
		empresaContinGen = baseDescuentos*porcentajeEmpresaContinGen/100;
		empresaDesempleo = baseDescuentos*porcentajeEmpresaDesempleo/100;
		empresaFormacion = baseDescuentos*porcentajeEmpresaFormacion/100;
		empresaAccidentes = baseDescuentos*porcentajeEmpresaAccidentes/100;
		empresaFOGASA = baseDescuentos*porcentajeEmpresaFOGASA/100;
		
		totalEmpresario = empresaContinGen + empresaDesempleo + empresaFormacion + empresaAccidentes + empresaFOGASA;
		
		costeTrabajador = totalDevengos+totalEmpresario;
	}

	public bbdd.Nomina getNominabbdd() {
		return nominabbdd;
	}

	public void setNominabbdd(bbdd.Nomina nominabbdd) {
		this.nominabbdd = nominabbdd;
	}
	
	
	
}