package objetos.empleado;

public class AtributoEmpleado {

	private String nombreAtributo;
	private Object valorAtributo;
	
	public AtributoEmpleado(String nombreAtributo, Object valorAtributo2) {
		this.nombreAtributo = nombreAtributo;
		this.valorAtributo = valorAtributo2;
	}

	public String getNombreAtributo() {
		return nombreAtributo;
	}
	public void setNombreAtributo(String nombreAtributo) {
		this.nombreAtributo = nombreAtributo;
	}
	public Object getValorAtributo() {
		return valorAtributo;
	}
	public void setValorAtributo(Object valorAtributo) {
		this.valorAtributo = valorAtributo;
	}
}