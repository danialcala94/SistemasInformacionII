package objetos;

import java.sql.Date;

public class Fecha {
	
	int dia;
	int mes;
	int anno;
	
	//Recibe una fecha en formato dd/mm/aaaa
	public Fecha(String fechaCompleta) {
		String[] separada = fechaCompleta.split("/");
		String diaS = separada[0];
		String mesS = separada[1];
		String annoS = separada[2];
		
		this.dia = Integer.parseInt(diaS);
		this.mes = Integer.parseInt(mesS);
		this.anno = Integer.parseInt(annoS);
	}
	
	public Fecha(int dia, int mes, int anno) {
		this.dia = dia;
		this.mes = mes;
		this.anno = anno;
	}
	
	public Fecha(String dia, String mes, String anno) {
		this.dia = Integer.parseInt(dia);
		this.anno = Integer.parseInt(anno);
		switch (mes) {
		case "Jan":
			this.mes = 1;
			break;
		case "Feb":
			this.mes = 2;
			break;
		case "Mar":
			this.mes = 3;
			break;
		case "Apr":
			this.mes = 4;
			break;
		case "May":
			this.mes = 5;
			break;
		case "Jun":
			this.mes = 6;
			break;
		case "Jul":
			this.mes = 7;
			break;
		case "Aug":
			this.mes = 8;
			break;
		case "Sep":
			this.mes = 9;
			break;
		case "Oct":
			this.mes = 10;
			break;
		case "Nov":
			this.mes = 11;
			break;
		case "Dec":
			this.mes = 12;
			break;
		}
	}

	public int getDia() {
		return dia;
	}

	public int getMes() {
		return mes;
	}

	public int getAnno() {
		return anno;
	}
	
	public String toString() {
		return dia+"/"+mes+"/"+anno;
	}
	
	/**
	 * Dadas una <strong>fechaUno</strong> y una <strong>fechaDos</strong>, devolver� TRUE si <strong>fechaUno</strong> es una fecha anterior a <strong>fechaDos</strong>,
	 * y FALSE si no lo es.
	 * @param fechaUno
	 * @param fechaDos
	 * @return
	 */
	public static boolean esAnteriorA(Fecha fechaUno, Fecha fechaDos) {
		boolean esAnterior = false;
		
		// A�o en formato YYYY
		String anoA_Str = rellenarConXCerosPorLaIzquierda(fechaUno.getAnno(), 4);
		String anoB_Str = rellenarConXCerosPorLaIzquierda(fechaDos.getAnno(), 4);
		// Mes en formato MM
		String mesA_Str = rellenarConXCerosPorLaIzquierda(fechaUno.getMes(), 2);
		String mesB_Str = rellenarConXCerosPorLaIzquierda(fechaDos.getMes(), 2);
		// D�a en formato DD
		String diaA_Str = rellenarConXCerosPorLaIzquierda(fechaUno.getDia(), 2);
		String diaB_Str = rellenarConXCerosPorLaIzquierda(fechaDos.getDia(), 2);
		
		int fechaA = Integer.parseInt(anoA_Str + mesA_Str + diaA_Str);
		int fechaB = Integer.parseInt(anoB_Str + mesB_Str + diaB_Str);
		
		if (fechaA < fechaB)
			esAnterior = true;
		
		return esAnterior;
	}
	
	/**
	 * Dadas una <strong>fechaUno</strong> y una <strong>fechaDos</strong>, devolver� TRUE si <strong>fechaUno</strong> es una fecha igual a <strong>fechaDos</strong>,
	 * y FALSE si no lo es.
	 * @param fechaUno
	 * @param fechaDos
	 * @return
	 */
	public static boolean esIgualA(Fecha fechaUno, Fecha fechaDos) {
		boolean esIgual = false;
		
		// A�o en formato YYYY
		String anoA_Str = rellenarConXCerosPorLaIzquierda(fechaUno.getAnno(), 4);
		String anoB_Str = rellenarConXCerosPorLaIzquierda(fechaDos.getAnno(), 4);
		// Mes en formato MM
		String mesA_Str = rellenarConXCerosPorLaIzquierda(fechaUno.getMes(), 2);
		String mesB_Str = rellenarConXCerosPorLaIzquierda(fechaDos.getMes(), 2);
		// D�a en formato DD
		String diaA_Str = rellenarConXCerosPorLaIzquierda(fechaUno.getDia(), 2);
		String diaB_Str = rellenarConXCerosPorLaIzquierda(fechaDos.getDia(), 2);
		
		int fechaA = Integer.parseInt(anoA_Str + mesA_Str + diaA_Str);
		int fechaB = Integer.parseInt(anoB_Str + mesB_Str + diaB_Str);
		
		if (fechaA == fechaB)
			esIgual = true;
		
		return esIgual;
	}
	
	/**
	 * Dadas una <strong>fechaUno</strong> y una <strong>fechaDos</strong>, devolver� TRUE si <strong>fechaUno</strong> es una fecha posterior a <strong>fechaDos</strong>,
	 * y FALSE si no lo es.
	 * @param fechaUno
	 * @param fechaDos
	 * @return
	 */
	public static boolean esPosteriorA(Fecha fechaUno, Fecha fechaDos) {
		boolean esPosterior = false;
		
		// A�o en formato YYYY
		String anoA_Str = rellenarConXCerosPorLaIzquierda(fechaUno.getAnno(), 4);
		String anoB_Str = rellenarConXCerosPorLaIzquierda(fechaDos.getAnno(), 4);
		// Mes en formato MM
		String mesA_Str = rellenarConXCerosPorLaIzquierda(fechaUno.getMes(), 2);
		String mesB_Str = rellenarConXCerosPorLaIzquierda(fechaDos.getMes(), 2);
		// D�a en formato DD
		String diaA_Str = rellenarConXCerosPorLaIzquierda(fechaUno.getDia(), 2);
		String diaB_Str = rellenarConXCerosPorLaIzquierda(fechaDos.getDia(), 2);
		
		int fechaA = Integer.parseInt(anoA_Str + mesA_Str + diaA_Str);
		int fechaB = Integer.parseInt(anoB_Str + mesB_Str + diaB_Str);
		
		if (fechaA > fechaB)
			esPosterior = true;
		
		return esPosterior;
	}
	
	/**
	 * Rellena con <strong>digitos</strong> ceros por la izquierda el <strong>valor</strong> dado.
	 * @param valor
	 * @param digitos
	 * @return
	 */
	private static String rellenarConXCerosPorLaIzquierda(int valor, int digitos) {
		String valor_Str = Integer.toString(valor);
		if (valor_Str.length() < digitos) {
			for (int i = valor_Str.length(); i < digitos; i++)
				valor_Str = "0" + valor_Str;
		}
		
		return valor_Str;
	}
	
	
	public Date toDate() {
		return new Date(anno, mes, dia);
	}
}
