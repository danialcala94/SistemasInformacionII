package objetos;

public class Trienio {
	double numTrienio;
	double importeMensual;
	
	public Trienio(double numTrienio, double importeMensual) {
		this.numTrienio=numTrienio;
		this.importeMensual=importeMensual;
	}

	public double getNumTrienio() {
		return numTrienio;
	}

	public void setNumTrienio(double numTrienio) {
		this.numTrienio = numTrienio;
	}

	public double getImporteMensual() {
		return importeMensual;
	}

	public void setImporteMensual(double importeMensual) {
		this.importeMensual = importeMensual;
	}

}
