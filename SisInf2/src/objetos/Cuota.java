package objetos;

public class Cuota {
	String nombreCuota;
	double porcentajeCuota;
	
	public Cuota(String nombreCuota, double porcentajeCuota) {
		this.nombreCuota = nombreCuota;
		this.porcentajeCuota = porcentajeCuota;
	}

	public String getNombreCuota() {
		return nombreCuota;
	}

	public void setNombreCuota(String nombreCuota) {
		this.nombreCuota = nombreCuota;
	}

	public double getPorcentajeCuota() {
		return porcentajeCuota;
	}

	public void setPorcentajeCuota(double porcentajeCuota) {
		this.porcentajeCuota = porcentajeCuota;
	}
	
}
