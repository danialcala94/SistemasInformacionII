package objetos;
import java.util.ArrayList;

import bbdd.Categorias;
import bbdd.Empresas;
import bbdd.Trabajadorbbdd;
import objetos.empleado.AtributoEmpleado;

public class Empleado {
	
	private int numeroFila;
	private String nif;
	private ArrayList<AtributoEmpleado> datosEmpleado;
	private SalarioCategoria salarioCategoria;
	private boolean prorrateo;
	private int mesCambioTrienio;
	private Trienio trienioSiguiente;
	private Trienio trienioAnterior;
	
	private Trabajadorbbdd trabajadorbbdd;
	private Empresas empresabbdd;
	private Categorias categoriabbdd;
	
	public Empleado(String nif, int numeroFila, String email, String codigoCuenta, String IBAN) {
		this.nif=nif;
		this.datosEmpleado = new ArrayList<AtributoEmpleado>();
		addAtributo("Email", email);
		addAtributo("CodigoCuenta", codigoCuenta);
		addAtributo("IBAN", IBAN);
		this.numeroFila = numeroFila;
	}
	
	public Empleado(String nif, int numeroFila) {
		this.numeroFila = numeroFila;
		this.nif = nif;
		this.datosEmpleado = new ArrayList<AtributoEmpleado>();
	}
	
	public void setListaAtributosEmpleado(ArrayList<AtributoEmpleado> lista) {
		this.datosEmpleado = lista;
	}
	
	public ArrayList<AtributoEmpleado> getListaAtributosEmpleado() {
		return this.datosEmpleado;
	}
	
	/**
	 * Devuelve el AtributoEmpleado, si existe, correspondiente al <strong>String nombreAtributo</strong> indicado. En caso de no existir, devuelve null.
	 * @param nombreAtributo
	 * @return
	 */
	public AtributoEmpleado getAtributo(String nombreAtributo) {
		for (int i = 0; i < datosEmpleado.size(); i++) {
			if (datosEmpleado.get(i).getNombreAtributo().equalsIgnoreCase(nombreAtributo))
				return datosEmpleado.get(i);
		}
		return null;
	}

	public void addAtributo(String nombreAtributo, Object valorAtributo) {
		//Comprueba si el atributo ya exist�a
		//Si exist�a sustituye el valor del atributo
		//Si no existia lo crea y almacena el valor
		for(int i=0; i<datosEmpleado.size();i++) {
			if(datosEmpleado.get(i).getNombreAtributo().equals(nombreAtributo)) {
				//Ya estaba guardado este atributo, se sustituye el valor y se termina el metodo
				datosEmpleado.get(i).setValorAtributo(valorAtributo);
				return;
			}
		}
		//Si se llega a este punto el atributo no estaba guardado, por lo que se a�ade
		datosEmpleado.add(new AtributoEmpleado(nombreAtributo, valorAtributo));
	}
	
	/**
	 * Elimina un atributo del empleado, dado un <strong>String nombreAtributo</strong>, si existe.
	 * @param nombreAtributo
	 */
	public void eliminarAtributo(String nombreAtributo) {
		for (int i = 0; i < datosEmpleado.size(); i++) {
			if (datosEmpleado.get(i).getNombreAtributo().toString().equalsIgnoreCase(nombreAtributo)) {
				datosEmpleado.remove(i);
				break;
			}
		}
	}
	
	/**
	 * Imprime por pantalla los atributos del empleado.
	 */
	public void mostrarAtributos() {
		for (int i = 0; i < datosEmpleado.size(); i++)
			System.out.println(datosEmpleado.get(i).getNombreAtributo() + " - " + datosEmpleado.get(i).getValorAtributo());
	}
	
	public String getNif() {
		return nif;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}
	
	public int getNumeroFila() {
		return numeroFila;
	}
	
	public ArrayList<AtributoEmpleado> getDatosEmpleado() {
		return this.datosEmpleado;
	}
	
	public void setDatosEmpleado(ArrayList<AtributoEmpleado> datosEmpleado) {
		this.datosEmpleado = datosEmpleado;
	}
	
	public String getNombre() {
		AtributoEmpleado atributoAux;
		for(int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if(atributoAux.getNombreAtributo().equalsIgnoreCase("Nombre")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}
	
	public String getApellido1() {
		AtributoEmpleado atributoAux;
		for(int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if(atributoAux.getNombreAtributo().equalsIgnoreCase("Apellido1")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}
	
	public String getApellido2() {
		AtributoEmpleado atributoAux;
		for (int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if (atributoAux.getNombreAtributo().equalsIgnoreCase("Apellido2")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}
	
	public String getEmpresa() {
		AtributoEmpleado atributoAux;
		for (int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if (atributoAux.getNombreAtributo().equalsIgnoreCase("Nombre empresa")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}
	
	public String getEmail() {
		AtributoEmpleado atributoAux;
		for (int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if (atributoAux.getNombreAtributo().equalsIgnoreCase("Email")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}
	
	public String getCuentaBancaria() {
		AtributoEmpleado atributoAux;
		for (int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if (atributoAux.getNombreAtributo().equalsIgnoreCase("CodigoCuenta")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}
	
	public String getPaisCuenta() {
		AtributoEmpleado atributoAux;
		for (int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if (atributoAux.getNombreAtributo().equalsIgnoreCase("Pais Origen Cuenta Bancaria")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}
	
	public void setCuentaBancaria(String cuentaBancaria) {
		AtributoEmpleado atributo;
		String nombreAtributo;
		for(int i=0;i<datosEmpleado.size();i++) {
			atributo=datosEmpleado.get(i);
			nombreAtributo=atributo.getNombreAtributo();
			if(nombreAtributo=="CodigoCuenta") {
				atributo.setValorAtributo(cuentaBancaria);
				break;
			}
		}
		addAtributo("CodigoCuenta", cuentaBancaria);
		//Si se acaba el for significa que el empleado no tenia indicada ninguna cuenta bancaria. No deber�a ocurrir nunca
	}
	
    public Empleado clone() {
		Empleado clone = new Empleado(this.nif, this.numeroFila);
		clone.setDatosEmpleado((ArrayList<AtributoEmpleado>) this.datosEmpleado.clone());
		clone.setSalarioCategoria(this.salarioCategoria);
		clone.setProrrateo(this.prorrateo);
		clone.setMesCambioTrienio(this.mesCambioTrienio);
		clone.setTrienioAnterior(this.trienioAnterior);
		clone.setTrienioSiguiente(this.trienioSiguiente);
		return clone;
    }
	
	public void setIBAN(String iban) {
		AtributoEmpleado atributo;
		String nombreAtributo;
		for(int i=0;i<datosEmpleado.size();i++) {
			atributo=datosEmpleado.get(i);
			nombreAtributo=atributo.getNombreAtributo();
			if(nombreAtributo=="IBAN") {
				atributo.setValorAtributo(iban);
				break;
			}
		}
		addAtributo("IBAN", iban);
		//Si se acaba el for significa que el empleado no tenia indicado ning�n IBAN.
	}
	
	public String getIBAN() {
		AtributoEmpleado atributoAux;
		for (int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if (atributoAux.getNombreAtributo().equalsIgnoreCase("IBAN")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}
	
	public String getCategoria() {
		AtributoEmpleado atributoAux;
		for (int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if (atributoAux.getNombreAtributo().equalsIgnoreCase("Categoria")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}
	
	public SalarioCategoria getSalarioCategoria() {
		return salarioCategoria;
	}

	public void setSalarioCategoria(SalarioCategoria salarioCategoria) {
		this.salarioCategoria = salarioCategoria;
	}
	

	public boolean isProrrateo() {
		return prorrateo;
	}

	public void setProrrateo(boolean prorrateo) {
		this.prorrateo = prorrateo;
	}
	
	public Fecha getFechaEntrada() {
		AtributoEmpleado atributoAux;
		for (int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if (atributoAux.getNombreAtributo().equalsIgnoreCase("FechaAltaEmpresa")) {
				return (Fecha) atributoAux.getValorAtributo();
			}
		}
		return null;
	}

	public int getMesCambioTrienio() {
		return mesCambioTrienio;
	}

	public void setMesCambioTrienio(int mesCambioTrienio) {
		this.mesCambioTrienio = mesCambioTrienio;
	}

	public Trienio getTrienioSiguiente() {
		return trienioSiguiente;
	}

	public void setTrienioSiguiente(Trienio trienioSiguiente) {
		this.trienioSiguiente = trienioSiguiente;
	}

	public Trienio getTrienioAnterior() {
		return trienioAnterior;
	}

	public void setTrienioAnterior(Trienio trienioAnterior) {
		this.trienioAnterior = trienioAnterior;
	}

	public boolean equals(Empleado empleado2) {
		if(empleado2.getNumeroFila()==numeroFila) {
			return true;
		}
		return false;
	}
	
	public String getCifEmpresa() {
		AtributoEmpleado atributoAux;
		for (int i=0; i<datosEmpleado.size(); i++) {
			atributoAux=datosEmpleado.get(i);
			if (atributoAux.getNombreAtributo().equalsIgnoreCase("Cif empresa")) {
				return (String) atributoAux.getValorAtributo();
			}
		}
		return null;
	}

	
	public Trabajadorbbdd getTrabajadorbbdd() {
		return trabajadorbbdd;
	}

	public void setTrabajadorbbdd(Trabajadorbbdd trabajadorbbdd) {
		this.trabajadorbbdd = trabajadorbbdd;
	}

	public Empresas getEmpresabbdd() {
		return empresabbdd;
	}

	public void setEmpresabbdd(Empresas empresabbdd) {
		this.empresabbdd = empresabbdd;
	}

	public Categorias getCategoriabbdd() {
		return categoriabbdd;
	}

	public void setCategoriabbdd(Categorias categoriabbdd) {
		this.categoriabbdd = categoriabbdd;
	}
	
	
}
