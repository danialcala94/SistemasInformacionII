package utilidades;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.omg.CORBA.DATA_CONVERSION;

import com.mysql.fabric.xmlrpc.base.Array;

import antlr.debug.SemanticPredicateAdapter;
import bbdd.Categorias;
import bbdd.Empresas;
import bbdd.Nomina;
import bbdd.Trabajadorbbdd;
import objetos.Empleado;
import objetos.SalarioCategoria;
import principal.HibernateUtils;

/*
 * Clase para los metodos que se encargan de convertir los objetos locales de nuestra aplicacion en objetos del tipo esperado por hibernate
 */
public class conversoresBD {
	//No es necesario un constructor de Empresas porque hasta ahora no teniamos un tipo de empresa
	//Asique al crearlo directamente lo crearemos del tipo necesario para la BD

	/*
	 * Transforma el tipo salarioCategoria en una Categoria de BD
	 */
	public static Categorias conversorCategoria(SalarioCategoria salarioCategoria) {
		Categorias categoria = new Categorias(salarioCategoria.getCategoria(), salarioCategoria.getSalarioBase(), salarioCategoria.getComplemento());
		salarioCategoria.setCategoriabbdd(categoria);
		return categoria;
	}
	
	/*
	 * Transforma una lista de salarioCategoria en una lista de Categorias de BD
	 */
	public static ArrayList<Categorias> conversorCategoria(ArrayList<SalarioCategoria> listaSalarioCategorias){
		ArrayList<Categorias> listaCategorias = new ArrayList<Categorias>();
		for(int i=0; i<listaSalarioCategorias.size(); i++) {
			listaCategorias.add(conversorCategoria(listaSalarioCategorias.get(i)));
		}
		return listaCategorias;
	}
	
	
	/*
	 * Transforma el tipo Empleado en un Trabajadorbbdd
	 */
	public static Trabajadorbbdd conversorEmpleado(Empleado empleado) {
		setCategoriaYEmpresa(empleado);
		int idCategoria = empleado.getCategoriabbdd().getIdCategoria();
		int idEmpresa = empleado.getEmpresabbdd().getIdEmpresa();
		java.sql.Date fechaAlta = new java.sql.Date(empleado.getFechaEntrada().getAnno()-1900, empleado.getFechaEntrada().getMes(), empleado.getFechaEntrada().getDia());

		Trabajadorbbdd trabajador = new Trabajadorbbdd(empleado.getNombre(), empleado.getApellido1(), empleado.getApellido2(), empleado.getNif(), empleado.getEmail(), fechaAlta, empleado.getCuentaBancaria(), empleado.getIBAN(), idEmpresa, idCategoria);
		empleado.setTrabajadorbbdd(trabajador);
		return trabajador;
	}
	
	/*
	 * Transforma una lista de salarioCategoria en una lista de Categorias de BD
	 */
	public static ArrayList<Trabajadorbbdd> conversorEmpleado(ArrayList<Empleado> listaEmpleados){
		ArrayList<Trabajadorbbdd> listaTrabajadores = new ArrayList<Trabajadorbbdd>();
		for(int i=0; i<listaEmpleados.size(); i++) {
			listaTrabajadores.add(conversorEmpleado(listaEmpleados.get(i)));
		}
		return listaTrabajadores;
	}
	
	/*
	 * Transforma el tipo Nomina que utilizabamos hasta ahora en una Nomina de la BD
	 */
	public static bbdd.Nomina conversorNomina (objetos.Nomina nominaLocal) {
		int idTrabajadorBD = nominaLocal.empleado.getTrabajadorbbdd().getIdTrabajador();
		
		bbdd.Nomina nominaBD = new bbdd.Nomina(
				nominaLocal.fecha.getMes(),					//Mes
				nominaLocal.fecha.getAnno(),				//Anio
				nominaLocal.numTrienios,					//NumTrienios
				nominaLocal.antiguedad,						//ImporteTrienios
				nominaLocal.salarioBase,					//ImporteSalarioMes
				nominaLocal.complementos,					//ImporteComplementoMes
				nominaLocal.prorrata,						//ValorProrrateo
				nominaLocal.brutoAnual,						//BrutoAnual
				nominaLocal.porcentajeIRPF,					//IRPF	%
				nominaLocal.irpf,							//ImporteIRPF
				nominaLocal.baseDescuentos,					//BaseEmpresario
				nominaLocal.porcentajeEmpresaContinGen,		//SSEmpresario	%
				nominaLocal.empresaContinGen,				//ImporteSSEmpresario
				nominaLocal.porcentajeEmpresaDesempleo,		//DesempleadoEmpresario	%
				nominaLocal.empresaDesempleo,				//ImporteDesempledoEmpresaario
				nominaLocal.porcentajeEmpresaFormacion,		//FormacionEmpresario	%
				nominaLocal.empresaFormacion,				//ImporteFormacionEmpresario
				nominaLocal.porcentajeEmpresaAccidentes,	//AccidentesEmpresario	%
				nominaLocal.empresaAccidentes,				//ImporteAccidentesEmpresario
				nominaLocal.porcentajeEmpresaFOGASA,		//FOGASAEmpresario %
				nominaLocal.empresaFOGASA,					//ImporteFOGASAEmpresario
				nominaLocal.porcentajeContinGen,			//SS Trabajador %
				nominaLocal.continGen,						//Importe SS Trabajdor
				nominaLocal.porcentajeDesempleo,			//Desempleo Trabajador %
				nominaLocal.desempleo,						//Importe desempleo Trabajador
				nominaLocal.porcentajeFormacion,			//Formacion Trabador %
				nominaLocal.formacion,						//Importe Formacion Trabajador
				nominaLocal.totalDevengos,					//BrutoNomina
				nominaLocal.liquidoAPercibir,				//LiquidoNomina
				nominaLocal.costeTrabajador,				//CosteTotalEmpresario
				idTrabajadorBD								//IdTrabajador
				);
		
		return nominaBD;
	}
	
	/*
	 * Transforma una lista de NominasLocales en una lista de Nominas de la BD
	 */
	public static ArrayList<bbdd.Nomina> conversorNomina(ArrayList<objetos.Nomina> listaNominasLocal){
		ArrayList<bbdd.Nomina> listaNominasBD = new ArrayList<bbdd.Nomina>();
		for(int i=0; i<listaNominasLocal.size(); i++) {
			listaNominasBD.add(conversorNomina(listaNominasLocal.get(i)));
		}
		return listaNominasBD;
	}
	
	
	public static void setCategoriaYEmpresa(Empleado empleado) {
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Query qry = session.createQuery("from Categorias where NombreCategoria like :categoria");
		qry.setString("categoria",'%'+empleado.getCategoria()+'%');
		ArrayList<Categorias> listaCategorias = (ArrayList<Categorias>) qry.list();
		
		empleado.setCategoriabbdd(listaCategorias.get(0));	//La categoria del empleado ser� la devuelta en la query (Devolver� una lista con solo 1 elemento)
		
		qry = session.createQuery("from Empresas where CIF like :cif");
		qry.setString("cif",'%'+empleado.getCifEmpresa()+'%');
		ArrayList<Empresas> listaEmpresas = (ArrayList<Empresas>) qry.list();
		
		empleado.setEmpresabbdd(listaEmpresas.get(0));	//La Empresa del empleado ser� la devuelta en la query (Devolver� una lista con solo 1 elemento)
	}
	 
}
