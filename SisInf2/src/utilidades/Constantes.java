package utilidades;

public class Constantes {
	public static int dni = 0;
	public static int apellido1 = 1;
	public static int apellido2 = 2;
	public static int nombre = 3;
	public static int email = 4;
	public static int categoria = 5;
	public static int nombreEmpresa = 6;
	public static int cifEmpresa = 7;
	public static int fechaAltaEmpresa = 8;
	public static int fechaAltaLaboral = 9;
	public static int fechaBajaLaboral = 10;
	public static int horasExtraForzadas = 11;
	public static int horasExtraVoluntarias = 12;
	public static int prorrataExtra = 13;
	public static int codigoCuenta = 14;
	public static int paisOrigenCuentaBancaria = 15;
	public static int iban = 15;
	
	/* Ficheros */
	public static final String FICHEROS_RUTA = "resources/";
	public static final String FICHERO_EXCEL_NOMBRE = "SistemasInformacionII.xlsx";
	public static final String FICHERO_EXCEL_RUTA = FICHEROS_RUTA + FICHERO_EXCEL_NOMBRE;
	public static final String FICHERO_XML_ERRORES = "Errores.xml";
	public static final String FICHERO_XML_CUENTAS_ERRONEAS = "cuentasErroneas.xml";
	public static final String FICHERO_XML_RUTA_ERRORES = FICHEROS_RUTA + FICHERO_XML_ERRORES;
	public static final String FICHERO_XML_RUTA_CUENTAS_ERRONEAS = FICHEROS_RUTA + FICHERO_XML_CUENTAS_ERRONEAS;
	
}
