package utilidades;

import java.util.ArrayList;

import objetos.Empleado;

/**
 * Clase encargada de realizar operaciones de comprobaci�n con los documentos NIF/NIE.
 * @author GAMING
 *
 */
public class NifNie {
	
	private static String nif;
	private static String nie;
	private static String []letras = {"T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E"};
	
	public static final String FORMATO_INCORRECTO = "Formato Incorrecto";
	public static final String INVALIDO = null;
	
	/**
	 * Corrige el NIF/NIE recibido como <strong>String identificador</strong> si es incorrecto y lo retorna, o retorna null si est� correcto.
	 * @param identificador
	 * @return
	 */
	public static String comprobarNif(String identificador) {
		// Si la longitud del identificador es distinta de 9 ya sabemos que va a estar mal y de devuelve Formato Incorrecto
		if (identificador.length() != 9)
			return FORMATO_INCORRECTO;
		
		String letra = identificador.substring(identificador.length() - 1);
		boolean esCorrecto = false;
		//En este if solo entrar�an aquellos dni que son diferentes de "" y que comienzan por numero, X, Y o Z.
		if (!identificador.equals("") && (Character.isDigit(identificador.charAt(0)) || identificador.charAt(0) == 'X' || identificador.charAt(0) == 'Y'|| identificador.charAt(0) == 'Z' )) {
			if (identificador.charAt(0) != 'Y' && identificador.charAt(0) != 'X' && identificador.charAt(0) != 'Z') {
				nif = identificador;
				String nuevoNif = nif.substring(0, nif.length()-1);
				int numeroNif;
				try {
					numeroNif = Integer.parseInt(nuevoNif);
				} catch (NumberFormatException e) {
					return FORMATO_INCORRECTO;
				}
				int resto = numeroNif % 23;
				// System.out.println("El resto de dividir -" + numeroNif + " entre: -- 23 es: " + resto);
				if (letra.equals(letras[resto])) {
					//esCorrecto = true;
					////System.out.println("El NIF es correcto. Ahora es:"+nif);
					return null;
				} else {
					//esCorrecto = false;
					nif = nuevoNif + letras[resto];
					////System.out.println("El NIF es incorrecto. Ahora es:"+nif);
					return nif;
				}
			} else {
				String letraAux = "";
				if (identificador.charAt(0) == 'X') {
					letraAux = "X";
					////System.out.println("El dni empieza por X.");
					nie = "0"+identificador.substring(1, identificador.length());
					
				} else if (identificador.charAt(0) == 'Y') {
					letraAux = "Y";
					//System.out.println("El dni empieza por Y.");
					nie = "1" + identificador.substring(1, identificador.length());
				} else if (identificador.charAt(0) == 'Z') {
					letraAux = "Z";
					//System.out.println("El dni empieza por Z.");
					nie = "2" + identificador.substring(1, identificador.length());	
				}
				String nuevoNie = nie.substring(0, nie.length()-1);
				int numeroNie;
				try {
					numeroNie = Integer.parseInt(nuevoNie);
				} catch (NumberFormatException e) {
					return FORMATO_INCORRECTO;
				}
				int resto = numeroNie % 23;
				// System.out.println("El resto de dividir -" + numeroNie + " entre: -- 23 es: " + resto);
				if (letra.equals(letras[resto])) {
					// System.out.println("El DNI es correcto. El numero es: " + nie);
					return null;
				} else {		
					nie = letraAux + nuevoNie.substring(1, nuevoNie.length()) + letras[resto]; 
					//System.out.println("El DNI es incorrecto. Ahora es:"+nie);
					return nie;
				}	
			}		
		} else {
			// Se devuelve Formato Incorrecto para que se almacene este empleado en la lista de errores y luego en el XML
			return FORMATO_INCORRECTO;
		}
	}
	
	/**
	 * Comprueba los NIFs/NIEs de los <strong>ArrrayList<Empleado> empleados</strong> y deriva al XML (a�ade a la lista <strong>ArrayList<Empleado> empleadosXML</strong>) aquellos con NIF/NIE vac�o pero con atributos, y elimina los completamente vac�os.
	 * @param empleados.
	 */
	public static ArrayList<Empleado> compruebaDNIVacio(ArrayList<Empleado> empleados, ArrayList<Empleado> empleadosXML) {
		ArrayList<Empleado> aux = (ArrayList<Empleado>) empleados.clone();
		
		for (int i = 0; i < empleados.size(); i++) {
			Empleado empleadoAux = empleados.get(i);
			if (empleadoAux.getNif() == "NIF Vacio") {
				// A�adir a la lista de errores
				if (empleadoAux.getDatosEmpleado().size() == 0) {
					// No es un empleado sino una l�nea en blanco y hay que eliminarlo porque no debe ir al XML
					aux.remove(empleadoAux);
				} else {
					// El empleado solo tiene el DNI en blanco
					//System.out.println("___"+empleadoAux.getNombre() +" (fila: "+empleadoAux.getNumeroFila() +") tiene atributos y solo el DNI en blanco");
					empleadosXML.add(empleadoAux);
				}
			}
		}
		return aux;
	}
	
	/**
	 * Comprueba si los NIEs/NIFs contenidos en <strong>ArrayList<Empleado> empleados</strong> son correctos. Si no tienen NIF/NIE (en blanco), se mandan a la lista para exportar a XML <strong>ArrayList<Empleado> empleadosXML</strong>, y si son incorrectos se corrigen
	 * y se pasan a la lista corregida <strong>ArrayList<Empleado> empleadosCorregidos</strong>.
	 * @param empleados
	 * @param empleadosXML
	 * @param empleadosCorregidos
	 */
	public static void compruebaDNICorrecto(ArrayList<Empleado> empleados, ArrayList<Empleado> empleadosXML, ArrayList<Empleado> empleadosCorregidos) {
		ArrayList<Empleado> aux = (ArrayList<Empleado>) empleados.clone();
		for (int i = 0; i < empleados.size(); i++) {
			Empleado empleadoAux = empleados.get(i);
			String retorno = NifNie.comprobarNif(empleadoAux.getNif());
			if (retorno != INVALIDO) {
				if (retorno == FORMATO_INCORRECTO) {
					//System.out.println(empleados.get(i).getNombre() + " tiene DNI con Formato Incorrecto");
					empleadosXML.add(empleadoAux);
				} else {
					// El DNI/NIF se ha corregido
					// Cambia en el Excel el valor del dni del empleado
					//System.out.println("Se ha sustituido " + empleadoAux.getNif() + " por " + retorno);
					empleadoAux.setNif(retorno);
					// A�adimos el empleado a la lista de correciones
					empleadosCorregidos.add(empleadoAux);
				}
			}
		}
	}
	
	/**
	 * Comprueba aquellos DNIs repetidos en el ArrayList<Empleado> que se le pasa, y los a�ade a la lista para exportar al XML <strong>ArrayList<Empleado> empleadosXML</strong>.
	 */
	public static void compruebaDNIRepetido(ArrayList<Empleado> empleados, ArrayList<Empleado> empleadosXML) {
		ArrayList<String> dnis = new ArrayList<String>();
		for (int i = 0; i < empleados.size(); i++) {
			Empleado empleadoAux = empleados.get(i);
			if (dnis.contains(empleadoAux.getNif())) {
				// A�adimos el empleado a la lista de empleados repetidos
				// A�adimos la fila en la que estaba el empleado a las filas a eliminar 
				//System.out.println(empleados.get(i).getNombre() + " tiene DNI repetido");
				empleadosXML.add(empleadoAux);
			} else {
				dnis.add(empleadoAux.getNif());
			}
		}
	}
}
