package utilidades;

import java.math.BigInteger;
import java.util.ArrayList;

import objetos.Empleado;

/**
 * Clase encargada de realizar operaciones con las cuentas bancarias de los empleados, y de realizar c�lculos como el del IBAN.
 * @author GAMING
 *
 */
public class OperacionesBancarias {
	
	private static int[] cadenaValores = {1, 2, 4, 8, 5, 10, 9, 7, 3, 6};
	private static String[] letras = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	private static String[] correspondencias = {"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35"};
	
	public static void comprobarNumeroCuenta(String numeroCuenta, String nacionalidad){
		String cuentaCorrecta = calcularCuentaBancariaCorrecta(numeroCuenta);
		String IBAN = calcularIBAN(cuentaCorrecta, nacionalidad);
	}
	
	public static void rellenaIBAN_Y_CuentaBancaria(ArrayList <Empleado> listaEmpleados, ArrayList<Empleado> empleadosErroresCuentas) {
		Empleado empleadoAux;
		for(int i=0; i<listaEmpleados.size(); i++) {
			empleadoAux = listaEmpleados.get(i);
			if(!rellenaIBAN_Y_CuentaBancaria(empleadoAux)) {
				//Si la cuenta no era la correcta se a�ade el empleado a la lista de los empleados que se van a imprimir en el XML
				empleadosErroresCuentas.add(empleadoAux);
			}
		}
	}
	
	
	public static boolean rellenaIBAN_Y_CuentaBancaria(Empleado empleado) {
		String cuentaBancariaEmpleado = empleado.getCuentaBancaria();
		String paisCuentaEmpleado = empleado.getPaisCuenta();
		String IBANCalculado;
		String cuentaBancaria = calcularCuentaBancariaCorrecta(cuentaBancariaEmpleado);
		empleado.setCuentaBancaria(cuentaBancaria);
		String iban = calcularIBAN(cuentaBancaria, paisCuentaEmpleado);
		//System.out.println("El IBAN calculado ha sido:"+iban);
		empleado.setIBAN(iban);
		//Si las cuentas no coinciden se devuelve false para que se a�ada el empleado a la lista de empleados con errores en la cuenta bancaria
		if(!cuentaBancaria.equalsIgnoreCase(cuentaBancariaEmpleado)) {
			empleado.addAtributo("CuentaBancariaErronea", cuentaBancariaEmpleado);
			return false;
		}
		return true;
	}
	
	
	/**
	 * Calcula, a partir de la <strong>String cadenaDigitos</strong> el digito de control correspondiente.
	 * @param cadenaDigitos
	 * @return
	 */
	private static String calcularDigitoControl(String cadenaDigitos) {
		String cadena;
		int sumatorio = 0;
		int resto = 0;
		String valorDigito;
		
		if (cadenaDigitos.length() == 8) {
			//System.out.println("La cadena tiene 8 caracteres");
			cadena = "00" + cadenaDigitos;
		} else {
			//System.out.println("La cadena tiene 10 caracteres");
			cadena = cadenaDigitos;
		}
		
		for (int i = 0; i < cadena.length(); i++) {
			//System.out.println("El valor de la cadena es: " + Character.getNumericValue(cadena.charAt(i)) + ". ---- EL valor fijo es:" + cadenaValores[i]);
			sumatorio += Character.getNumericValue(cadena.charAt(i)) * cadenaValores[i];
		}
		
		resto = sumatorio % 11;
		valorDigito = String.valueOf(11 - resto);
		if (valorDigito.equalsIgnoreCase("10")) {
			valorDigito = "1";
		} else if (valorDigito.equalsIgnoreCase("11")) {
			valorDigito = "0";
		}
		return valorDigito;
	}
	
	/**
	 * Comprueba si la cuenta <strong>String numeroCuenta</strong> es correcta, devolviendo true, o si es incorrecta, devolviendo false.
	 * @param numeroCuenta
	 * @return
	 */
	public static String calcularCuentaBancariaCorrecta(String numeroCuenta) {
		String primerDigitoControl = calcularDigitoControl(numeroCuenta.substring(0,8));
		String segundoDigitoControl = calcularDigitoControl(numeroCuenta.substring(10, numeroCuenta.length()));
		
		if (primerDigitoControl !=  numeroCuenta.substring(8, 9) || segundoDigitoControl != numeroCuenta.substring(9, 10)) {
			numeroCuenta = numeroCuenta.substring(0,8) + primerDigitoControl + segundoDigitoControl + numeroCuenta.substring(10,numeroCuenta.length());
		}
		
		return numeroCuenta;
	}
	
	/**
	 * Calcula el IBAN de la cuenta <strong>String numeroCuenta</strong>, considerando tambi�n la <strong>String nacionalidad</strong> indicada.
	 * @param numeroCuenta
	 * @param nacionalidad
	 * @return
	 */
	private static String calcularIBAN(String numeroCuenta, String nacionalidad){
		String valor1 = nacionalidad.substring(0, 1);
		String valor2 = nacionalidad.substring(1, 2);
		String numero1 = "";
		String numero2 = "";
		
		for (int i = 0; i < letras.length; i++){
			if (letras[i].equals(valor1)) {
				numero1 = correspondencias[i];
			} else if (letras[i].equals(valor2)) {
				numero2 = correspondencias[i];
			}
		}
		
		String cuenta = numeroCuenta + numero1 + numero2 + "00";
		BigInteger bigCuenta = new BigInteger(cuenta);
		BigInteger big97 = new BigInteger("97");
		
		BigInteger restoBig = bigCuenta.mod(big97);
		int resto =  restoBig.intValue();
		int diferencia = 98 - resto;
		String valorDif = String.valueOf(diferencia);
		if(valorDif.length() < 2){
			valorDif = "0"+valorDif;
		}
		String IBAN = nacionalidad + valorDif + numeroCuenta;
		
		return IBAN;
	}
	
	private static boolean comprobarIBAN(String iban){
		String valor1 = iban.substring(0, 1);
		String valor2 = iban.substring(1,2);
		String valorFinal = iban.substring(2,4);
		String numero1 = "";
		String numero2 = "";
		boolean comprobacion = false;
		
		for (int i = 0; i < letras.length; i++){
			if (letras[i].equals(valor1)) {
				numero1 = correspondencias[i];
			} else if (letras[i].equals(valor2)) {
				numero2 = correspondencias[i];
			}
		}
		String numeroFinal = iban.substring(4,iban.length()) + numero1 + numero2 + valorFinal;
		
		BigInteger bigCuenta = new BigInteger(numeroFinal);
		BigInteger big97 = new BigInteger("97");
		BigInteger restoBig = bigCuenta.mod(big97);
		int resto =  restoBig.intValue();
		
		if(resto == 1){
			comprobacion = true;
		}else{
			comprobacion = false;
		}
		
		return comprobacion;
	}


}
