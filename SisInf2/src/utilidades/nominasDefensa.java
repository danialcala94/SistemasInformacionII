package utilidades;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbdd.Categorias;
import bbdd.Empresas;
import bbdd.Nomina;
import bbdd.Trabajadorbbdd;
import principal.HibernateUtils;

public class nominasDefensa {

	public static bbdd.Nomina nominaLiquidoMin(ArrayList<bbdd.Nomina> listaNominas){
		bbdd.Nomina nominaMin = null;
		bbdd.Nomina nominaAct;
		for(int i=0; i<listaNominas.size(); i++) {
			nominaAct = listaNominas.get(i);
			if(nominaMin == null) {
				nominaMin = nominaAct;
			}else {
				if(nominaAct.getLiquidoNomina() < nominaMin.getLiquidoNomina()) {
					if(nominaAct.getLiquidoNomina() != 0) {
						nominaMin = nominaAct;
					}
				}
			}
		}
		return nominaMin;
	}
	
	
	public static bbdd.Nomina nominaLiquidoMinBD(){
    	SessionFactory sf = HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Query qry = session.createQuery("from Nomina");
		ArrayList<Nomina> listaNominas= (ArrayList<Nomina>) qry.list();
		session.close();
		
		bbdd.Nomina nominaMinBD = nominaLiquidoMin(listaNominas);
		return nominaMinBD;
	}
	
	
	public static bbdd.Trabajadorbbdd trabajadorFromID(int idTrabajador){
		SessionFactory sf = null;
		sf=HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
		bbdd.Trabajadorbbdd trabajador = session.get(bbdd.Trabajadorbbdd.class, idTrabajador);
		tx.commit();
		session.close();
		return trabajador;	
	}
	
	
	public static bbdd.Categorias categoriaFromID(int idCategoria){
		SessionFactory sf = null;
		sf=HibernateUtils.getSessionFactory();
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
		bbdd.Categorias categoria = session.get(bbdd.Categorias.class, idCategoria);
		tx.commit();
		session.close();
		return categoria;
	}
	
}