package utilidades;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import objetos.Cuota;
import objetos.Empleado;
import objetos.Fecha;
import objetos.Nomina;
import objetos.Retencion;
import objetos.Trienio;

public class calcularNominas {
	
	/*
	 	1. Contingencias comunes (SS) ---> 4,7%
	 	2. Formacion ---> 0,1%
	 	3. Desempleo ---> 1,6%
	 	4. IRPF ---> 12,08%

	 */
	
	public static Fecha fechaNomina;//
	public static Empleado empleadoNomina;//
	public static double brutoAnualNomina;//
	public static double salarioBaseNomina;//
	public static double complementoNomina;//
	public static double prorrateoNomina;//
	public static double porcentajeIRPFNomina;//
	public static double baseDescuentosNomina;//
	
	
	
	
	public static void calcularNominas(ArrayList<Empleado> listaEmpleados, Fecha fechaConsola, ArrayList <Retencion> retenciones, ArrayList<Cuota> listaCuotasNominas, ArrayList<Nomina> listaNominas){
		for (int i = 0; i < listaEmpleados.size(); i++) {
		if(listaEmpleados.get(i).getNif().equalsIgnoreCase("01234567L")) {
				//System.out.println("Break");
		}
		//for(int i=0; i<6;i++) {
			calcularLiquidoMensualEmpleado(listaEmpleados.get(i), fechaConsola, retenciones);
			Nomina nomina = new Nomina(fechaNomina, empleadoNomina, brutoAnualNomina, salarioBaseNomina, prorrateoNomina, complementoNomina, listaCuotasNominas, porcentajeIRPFNomina, baseDescuentosNomina);
			listaNominas.add(nomina);
			System.out.println("\t�Se ha generado la nomina del trabajador con DNI:"+empleadoNomina.getNif());
			// Tenemos que controlar que se generen las n�minas extra necesarias (si es JUNIO o DICIEMBRE)
			if (!empleadoNomina.isProrrateo() && (fechaNomina.getMes() == 6 || fechaNomina.getMes() == 12)) {
				//System.out.println("Generando n�mina EXTRA...");
				// Crear un objeto nomina con 
				Nomina nominaExtra = new Nomina(fechaNomina, empleadoNomina, brutoAnualNomina, salarioBaseNomina, prorrateoNomina, complementoNomina, listaCuotasNominas, porcentajeIRPFNomina, baseDescuentosNomina, true);
				listaNominas.add(nominaExtra);
				System.out.println("\t�Se ha generado la nomina extra del trabajador con DNI:"+empleadoNomina.getNif());
			}
			
			//System.out.println("calculado:"+i);
		}
	}
	
	
	public static void calcularLiquidoMensualEmpleado(Empleado empleado, Fecha fechaConsola, ArrayList<Retencion> retenciones){
		empleadoNomina = empleado;
		fechaNomina = fechaConsola;
		double salarioBase = empleado.getSalarioCategoria().getSalarioBase();
		double complemento = empleado.getSalarioCategoria().getComplemento();
		//System.out.println("NOMBRE: " + empleado.getNombre()+ empleado.getApellido1()+empleado.getApellido2());
		
		//if(empleado.getTrienioAnterior() != null)
			//System.out.println("T.ANTERIOR -----> "+ empleado.getTrienioAnterior().getNumTrienio()+ " VALOR +++++++ "+empleado.getTrienioAnterior().getImporteMensual());
		//System.out.println("----------------!!!!!!!!!!!!!!!--------------");
		//if(empleado.getTrienioSiguiente() != null)
			//System.out.println("T.SIGUIENTE -----> "+ empleado.getTrienioSiguiente().getNumTrienio()+" VALOR +++++++ "+empleado.getTrienioSiguiente().getImporteMensual());
		
		double trienioAnual = calcularTrienioAnual(empleado.getTrienioAnterior(), empleado.getTrienioSiguiente(), empleado.getMesCambioTrienio());
		String fechaAltaString = empleado.getFechaEntrada().toString();
		String anio = fechaAltaString.substring(fechaAltaString.length()-4, fechaAltaString.length());
		int anioAlta = Integer.parseInt(anio);
		int mesAlta = empleado.getFechaEntrada().getMes();
		int mesCambio = empleado.getMesCambioTrienio();
		//System.out.println("MES DE CAMBIO +-+-+-+-+"+empleado.getMesCambioTrienio());
		
		int anioConsola = fechaConsola.getAnno();
		int mesConsola = fechaConsola.getMes();
		double porcentaje;
		double devengo;
		
		if(anioAlta != anioConsola){
			porcentajeIRPFNomina = calcularPorcentajaIRPF(calcularBrutoAnualDiferenteAno(salarioBase, complemento, trienioAnual), retenciones);
			
			if(mesCambio != -1){
				//Hay cambio de trienio
				double trienioMes = 0;
				double trienioExtra = 0;
				if(mesConsola < mesCambio){
					
					if(empleado.getTrienioAnterior() != null){
						//Cambio de trienio diferente al primero
						trienioMes = empleado.getTrienioAnterior().getImporteMensual();		
					}else{
						//Primer cambio de trienio
						//trienioMes = empleado.getTrienioSiguiente().getImporteMensual();
						trienioMes = 0;
					}
					
					
					if(mesCambio<=6){
						trienioExtra = empleado.getTrienioSiguiente().getImporteMensual();
					}else{
						//COmprobar esto
						if(mesConsola < 6){
							trienioExtra = trienioMes;
							//trienioExtra = empleado.getTrienioAnterior().getImporteMensual();
						}else if(mesConsola == 6){
							//trienioExtra = empleado.getTrienioSiguiente().getImporteMensual();
							trienioExtra = empleado.getTrienioSiguiente().getImporteMensual();
						}else if(mesConsola > 6){
							trienioExtra = empleado.getTrienioSiguiente().getImporteMensual();
						}
					}
					
				}else if(mesConsola >= mesCambio){
					trienioMes = empleado.getTrienioSiguiente().getImporteMensual();
					trienioExtra = empleado.getTrienioSiguiente().getImporteMensual();
				}
				
				devengo  = calcularDevengoConCambioTrienio(empleado.isProrrateo(), salarioBase, complemento, trienioMes, trienioExtra);
				
				if(empleado.isProrrateo() == true){
					//Si hay prorrateo, la base sobre la que se har�n los calculos es la misma para irpf que para ss, desempleo y formacion.
					baseDescuentosNomina = devengo;
				}else{
					//Si no hay prorrateo, debemos de prorratear para realizar los calculos de los impuestos ss, formacion y desempleo.
					baseDescuentosNomina = calcularDevengoConProrrateo(devengo, salarioBase, complemento, trienioExtra);
				}
				
				
			}else{
				//No hay cambio de trienio
				double trienio = 0; 
				if(empleado.getTrienioAnterior() != null){
					//En caso de que no sea el primer trienio, se cogeria siempre el trienio anterior.
					trienio = empleado.getTrienioAnterior().getImporteMensual();
					devengo = calcularDevengoSinCambioTrienio(empleado.isProrrateo(), salarioBase, complemento, trienio);
				}else{
					//Si el trienioAnterior es null, quiere decir que es el primer trienio y por lo tanto el trienio seria igual al treinioSiguiente.
					//TODO
					//Hecho por Martin el if else, no se si est� bien
					if(empleado.getTrienioSiguiente()!=null) {
						trienio = empleado.getTrienioSiguiente().getImporteMensual();
					}else {
						trienio = 0;
					}
					devengo = calcularDevengoSinCambioTrienio(empleado.isProrrateo(), salarioBase, complemento, trienio);
				}
				
				if(empleado.isProrrateo() == true){
					//Si hay prorrateo, la base sobre la que se har�n los calculos es la misma para irpf que para ss, desempleo y formacion.
					baseDescuentosNomina = devengo;
				}else{
					//Si no hay prorrateo, debemos de prorratear para realizar los calculos de los impuestos ss, formacion y desempleo.
					baseDescuentosNomina = calcularDevengoConProrrateo(devengo, salarioBase, complemento, trienio);
				}
			}	
		}else{
			porcentajeIRPFNomina = calcularPorcentajaIRPF(calcularBrutoAnualMismoAno(salarioBase, complemento, mesAlta), retenciones);
			devengo = calcularDevengoMismoAno(empleado.isProrrateo(), salarioBase, complemento, mesConsola);
			
			if(empleado.isProrrateo() == true){
				//Si hay prorrateo, la base sobre la que se har�n los calculos es la misma para irpf que para ss, desempleo y formacion.
				baseDescuentosNomina = devengo;
			}else{
				//Si no hay prorrateo, debemos de prorratear para realizar los calculos de los impuestos ss, formacion y desempleo.
				baseDescuentosNomina = calcularDevengoConProrrateoMismoAno(devengo, salarioBase, complemento);
			}
		}
		
	}
	/**
	 * Calculamos el valor de los trienios anual.
	 * @param anterior
	 * @param posterior
	 * @param mes
	 * @return
	 */
	public static double calcularTrienioAnual(Trienio anterior, Trienio posterior, int mes){
		double sumaTrieniosAnual = 0;
		
		if(anterior == null && posterior == null){
			//No hay trienios
			sumaTrieniosAnual = 0;
		}else if(anterior == null && posterior != null){
			//Se produce el primer cambio de trienio
			if(mes != 12){
				/*if(mes == 6){
					sumaTrieniosAnual = posterior.getImporteMensual()*9;
				}else{
					sumaTrieniosAnual = posterior.getImporteMensual()*(14-mes+1);
				}*/
				if(mes == 6){
					sumaTrieniosAnual = posterior.getImporteMensual()*9;
				}else if(mes < 6){
					sumaTrieniosAnual = posterior.getImporteMensual()*(14-mes+1);
				}else if(mes > 6){
					sumaTrieniosAnual = posterior.getImporteMensual()*(14-mes);
				}
			}else{
				sumaTrieniosAnual=posterior.getImporteMensual()*2;
			}
		}else if(anterior != null && posterior == null){
			//Ese a�o no hay cambio de trienio
			sumaTrieniosAnual = 14*anterior.getImporteMensual();
		}else{
			//Cambio de trienio normal
			if(mes != 12){
				if(mes < 7){
					sumaTrieniosAnual = anterior.getImporteMensual()*(mes-1) + posterior.getImporteMensual() * (14-mes+1);
				}else if(mes == 7){
					sumaTrieniosAnual = anterior.getImporteMensual()*mes + posterior.getImporteMensual()*(14-mes);
				}else{
					sumaTrieniosAnual = anterior.getImporteMensual()*mes + posterior.getImporteMensual()*(14-mes);
				}
			}else{
				sumaTrieniosAnual = 12*anterior.getImporteMensual() + 2*posterior.getImporteMensual();
			}
		}
		return sumaTrieniosAnual;
	}
	
	

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////// BRUTO ANUAL /////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public static double calcularBrutoAnualDiferenteAno(double salarioBase, double complementos, double trienios){
		brutoAnualNomina = salarioBase+complementos+trienios; 
		return brutoAnualNomina;
	}
	
	
	public static double calcularBrutoAnualMismoAno(double salarioBase, double complementos, int mes){
		double salario = salarioBase*(12-mes+1)/14;
		double complemento = complementos*(12-mes+1)/14;
		double extra = salarioBase/14 + complementos/14;
		double totalExtra = 0;
		if(mes < 6){
			//Cobra la de diciembre entera mas lo que le corresponda de la primero
			totalExtra = extra + (6-mes)*extra/6;
		}else if(mes == 6){
			//Cobra la de diciembre
			totalExtra = extra;
		}else{
			//Solo cobra una parte de la de 
			totalExtra = (12-mes)*extra/6;
		}
		
		brutoAnualNomina = salario + complemento + totalExtra;
		
		return brutoAnualNomina;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////// BRUTO MENSUAL ///////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static double calcularDevengoMismoAno(boolean prorrateo, double salarioBase, double complemento, int mes){
		double devengo = 0;
		salarioBaseNomina = salarioBase/14;
		complementoNomina = complemento/14;
		if(prorrateo == false){
			prorrateoNomina = 0;
			devengo = salarioBaseNomina + complementoNomina;
		}else{
			prorrateoNomina = (salarioBaseNomina + complementoNomina)/6;
			devengo = salarioBaseNomina + complementoNomina + prorrateoNomina;
		}
		
		return devengo;
	}
	
	
	public static double calcularDevengoSinCambioTrienio(boolean prorrateo, double salarioBase, double complemento, double trienioMes){
		double devengo = 0;
		salarioBaseNomina = salarioBase/14;
		complementoNomina = complemento/14;
		double trienioMensual = trienioMes;
		
		if(prorrateo == false){
			prorrateoNomina = 0;
			devengo = salarioBaseNomina + complementoNomina+ trienioMensual;
		}else{
			prorrateoNomina =  (salarioBaseNomina+ complementoNomina+trienioMes)/6;
			devengo = salarioBaseNomina + complementoNomina + trienioMensual + prorrateoNomina;
		}
		return devengo;
	}
	

	public static double calcularDevengoConCambioTrienio(boolean prorrateo, double salarioBase, double complemento, double trienioMes, double trienioExtra){
		double devengo = 0;
		salarioBaseNomina = salarioBase/14;
		complementoNomina = complemento/14;
		double trienioMensual = trienioMes;
		
		if(prorrateo == false){
			prorrateoNomina = 0;
			devengo = salarioBaseNomina + complementoNomina+ trienioMensual;
		}else{
			prorrateoNomina = (salarioBaseNomina + complementoNomina + trienioExtra)/6;
			devengo = salarioBaseNomina + complementoNomina + trienioMensual + prorrateoNomina;
		}
		return devengo;
	}
	
	public static double calcularDevengoConProrrateoMismoAno(double devengoSinProrrateo, double salarioBase, double complementos){
		double devengoProrrateo = 0;
		double salarioBaseMensual = salarioBase/14;
		double complementoMensual = complementos/14;
		devengoProrrateo = devengoSinProrrateo + (salarioBaseMensual + complementoMensual)/6;
		baseDescuentosNomina = devengoProrrateo;
		return devengoProrrateo;
	}
	
	public static double calcularDevengoConProrrateo(double devengoSinProrrateo, double salarioBase, double complementos,double trienioExtra){
		double devengoProrrateo = 0;
		double salarioBaseMensual = salarioBase/14;
		double complementoMensual = complementos/14;
		
		devengoProrrateo = devengoSinProrrateo + (salarioBaseMensual+complementoMensual+trienioExtra)/6;
		baseDescuentosNomina = devengoProrrateo;
		return devengoProrrateo;
	}
	

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////// IMPUESTOS /////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public static double calcularImpuestoSeguridadSocial(double salarioBrutoMensualConProrrateo){
		double impuestoSS = (4.7*salarioBrutoMensualConProrrateo)/100;
		return impuestoSS;
	}
	
	public static double calcularImpuestoDesempleo(double salarioBrutoMensualConProrrateo){
		double impuestoDesempleo = (1.6*salarioBrutoMensualConProrrateo)/100;
		return impuestoDesempleo;
	}
	
	public static double calcularImpuestoFormacion(double salarioBrutoMensualConProrrateo){
		double impuestoFormacion = (0.1*salarioBrutoMensualConProrrateo)/100;
		return impuestoFormacion;
	}

	
	/*
	 * El IRPF se calcula siempre sobre el devengo, sobre lo que obtenemos directamente. Sin embargo, los otros tres impuestos son 
	 * siempre sobre la nomina prorrateada, por lo tanto hay que hacer el prorrateo s� o s�.
	 */
	public static double calcularImpuestoIRPF(double salarioMensual, double porcentaje){
		double impuestoIRPF = (salarioMensual*porcentaje)/100;
		return impuestoIRPF;
	}
	
	public static double calcularPorcentajaIRPF(double brutoAnual, ArrayList<Retencion> retenciones){
		double porcentaje = 0;
		for(int i=0;i<retenciones.size();i++){
			if(brutoAnual <=  retenciones.get(i).getBruto()){
				porcentaje = retenciones.get(i).getPorcentajeRetencion();
				break;
			}
			if( i == (retenciones.size()-1) && porcentaje == 0){ 
				porcentaje = retenciones.get(retenciones.size()-1).getPorcentajeRetencion();
			}
		}
		
		return porcentaje;
	}
	
	public static double calcularDeducciones(double impuestoIRPF, double impuestoSS, double impuestoFormacion, double impuestoDesempleo){
		return impuestoIRPF + impuestoSS + impuestoFormacion + impuestoDesempleo;
	}
	
	public static double calcularLiquidoMensual(double devengo, double deduccion){
		return devengo - deduccion;
	}
	
	/*public static int calcularMes(Date fecha){
		String formato="MM";
		SimpleDateFormat dateFormat = new SimpleDateFormat(formato);
		return Integer.parseInt(dateFormat.format(fecha));
	}*/
	
	public static int calcularMesStringConsola(String fecha){
		String mesString = fecha.substring(0,2);
		int mes = Integer.parseInt(mesString);
		return mes;
	}
	
	public static int calcularAnioStringConsola(String fecha){
		String anioString = fecha.substring(fecha.length()-4, fecha.length());
		int anio = Integer.parseInt(anioString);
		return anio;
	}

	
	
	
	
	
}
