package utilidades;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;

import org.apache.commons.collections4.Trie;

import bbdd.Empresas;
import objetos.Empleado;
import objetos.Fecha;
import objetos.Retencion;
import objetos.SalarioCategoria;
import objetos.Trienio;
import objetos.empleado.AtributoEmpleado;
import principal.Nucleo;



/**
 * Clase para la realizaci�n de diferentes operaciones y comprobaciones sobre los objetos.
 * @author GAMING
 *
 */
public class Utilidades {
	
	
	/**
	 * Devuelve false si el <strong>String texto</strong> indicado solo tiene espacios o no tiene absolutamente nada. De lo contrario devuelve true.
	 * @param texto
	 * @return
	 */
	public static boolean campoValido(String texto) {
		if(texto != null) {
			for(int i=0;i<texto.length();i++) {
				if(texto.charAt(i) != ' ') {
					return true;
				}
			}
		}
		return false;
	}
	
	/* 	EXPORTAR A XML	*/
	/********************/
	
	/**
	 * Modifica el <strong>ArrayList<Empleado> empleados</strong> para que tenga los campos que se quieren exportar al XML (versi�n Pr�ctica 1).
	 * @param empleados
	 */
	public static ArrayList<Empleado> darFormatoXML(ArrayList<Empleado> empleados) {
		// Ordenamos por numeroFila
		empleados.sort(new Comparator<Empleado>() {
			public int compare(Empleado o1, Empleado o2) {
				if (o1.getNumeroFila() > o2.getNumeroFila()) {
					return 1;
				}
				return -1;
			}
		});
		
		// Eliminamos los campos que no se quieren mostrar
		for (int i = 0; i < empleados.size(); i++)
			eliminarAtributosNoXML(empleados.get(i));
		
		// Eliminamos empleados duplicados
		eliminarEmpleadosDuplicados(empleados);
		
		// Cambiamos el nombre a los campos
		for (int i = 0; i < empleados.size(); i++) {
			for (int j = 0; j < empleados.get(i).getDatosEmpleado().size(); j++) {
				String nombreCampo = empleados.get(i).getDatosEmpleado().get(j).getNombreAtributo().toString();
				
		    	// Se sustituye el nombre por el que se le quiere dar en el XML
				switch (nombreCampo) {
				case "Apellido1":
					nombreCampo = "PrimerApellido";
					break;
				case "Apellido2":
					nombreCampo = "SegundoApellido";
					break;
				case "Nombre empresa":
					nombreCampo = "Empresa";
					break;
				}
		    	
				empleados.get(i).getDatosEmpleado().get(j).setNombreAtributo(nombreCampo);
			}
		}
		
		return empleados;
	}
	
	/**
	 * Elimina aquellos empleados que aparezcan duplicados (busca coincidencia en NumeroFila).
	 * @param empleados
	 */
	private static void eliminarEmpleadosDuplicados(ArrayList<Empleado> empleados) {
		for (int i = 0; i < empleados.size(); i++) {
			for (int j = i + 1; j < empleados.size(); j++) {
				if (empleados.get(i).getNumeroFila() == empleados.get(j).getNumeroFila()) {
					//System.err.println("[DEBUG] El empleado " + empleados.get(i).getNombre() + " aparece duplicado. �Se debe corregir este fallo!");
					empleados.remove(j);
					j--;
				}
			}
		}
	}
	
	/**
	 * Elimina del <strong>Empleado empleado</strong> aquellos atributos que no queremos exportar al XML.
	 * @param empleado
	 */
	private static void eliminarAtributosNoXML(Empleado empleado) {
		for (int i = 0; i < empleado.getDatosEmpleado().size(); i++) {
			// Si el nombre no coincide con ninguno de los 5 que queremos guardar, lo eliminamos.
			String nombre = empleado.getDatosEmpleado().get(i).getNombreAtributo().toString();
			// Se compara con las que se quieren mantener (nombre original en el Excel), quitando los espacios
			if (!nombre.equalsIgnoreCase("Nombre") &&
					!nombre.equalsIgnoreCase("Apellido1") &&
					!nombre.equalsIgnoreCase("Apellido2") &&
					!nombre.equalsIgnoreCase("Categoria") &&
					!nombre.equalsIgnoreCase("Nombre empresa")) {
				empleado.eliminarAtributo(empleado.getDatosEmpleado().get(i).getNombreAtributo().toString());
				// Si elimino el elemento, uno pasar� a ocupar su lugar y como la i aumenta, no lo comprobar�, por eso decremento
				i--;
			}
		}
	}
	
	/**
	 * Elimina del <strong>Empleado empleado</strong> aquellos atributos que no queremos exportar al XML.
	 * Version P2
	 * @param empleado
	 */
	private static void eliminarAtributosNoXML_P2(Empleado empleado) {
		for (int i = 0; i < empleado.getDatosEmpleado().size(); i++) {
			// Si el nombre no coincide con ninguno de los 5 que queremos guardar, lo eliminamos.
			String nombre = empleado.getDatosEmpleado().get(i).getNombreAtributo().toString();
			// Se compara con las que se quieren mantener (nombre original en el Excel), quitando los espacios
			if (!nombre.equalsIgnoreCase("Nombre") &&
					!nombre.equalsIgnoreCase("PrimerApellido") &&
					!nombre.equalsIgnoreCase("SegundoApellido") &&
					!nombre.equalsIgnoreCase("Apellido1") &&
					!nombre.equalsIgnoreCase("Apellido2") &&
					!nombre.equalsIgnoreCase("Nombre empresa") &&
					!nombre.equalsIgnoreCase("Empresa") &&
					!nombre.equalsIgnoreCase("CuentaBancariaErronea") &&
					!nombre.equalsIgnoreCase("IBAN")) {
				empleado.eliminarAtributo(empleado.getDatosEmpleado().get(i).getNombreAtributo().toString());
				// Si elimino el elemento, uno pasar� a ocupar su lugar y como la i aumenta, no lo comprobar�, por eso decremento
				i--;
			}
		}
	}
	
	/**
	 * Comprueba si el <strong>String texto</strong> que se le pasa solo tiene espacios (devuelve true), o tiene algo m�s (devuelve false)
	 * @param texto
	 * @return
	 */
	public static boolean soloEspacios(String texto) {
		for (int i = 0; i < texto.length(); i++) {
			if (texto.charAt(i) != ' ')
				return false;
		}
		return true;
	}
	
	/**
	 * Elimina las tildess de un String pasado como parametro y lo devuelve en el return
	 */
	public static String eliminaTildes(String texto) {
		String aux = "";
		for(int i=0; i<texto.length(); i++) {
			switch (texto.charAt(i)) {
			case '�':
				aux = aux + 'a';
				break;
			case '�':
				aux = aux + 'e';
				break;
			case '�':
				aux = aux + 'i';
				break;
			case '�':
				aux = aux + 'o';
				break;
			case '�':
				aux = aux + 'u';
				break;
			default:
				aux = aux + texto.charAt(i);
				break;
			}
		}
		return aux;
	}


	/**
	 * Modifica el <strong>ArrayList<Empleado> empleados</strong> para que tenga los campos que se quieren exportar al XML (versi�n Pr�ctica 1).
	 * @param empleados
	 */
	public static ArrayList<Empleado> darFormatoXML_P2(ArrayList<Empleado> empleados) {
		// Ordenamos por numeroFila
		empleados.sort(new Comparator<Empleado>() {
			public int compare(Empleado o1, Empleado o2) {
				if (o1.getNumeroFila() > o2.getNumeroFila()) {
					return 1;
				}
				return -1;
			}
		});
		
		// Eliminamos los campos que no se quieren mostrar
		for (int i = 0; i < empleados.size(); i++)
			eliminarAtributosNoXML_P2(empleados.get(i));
		
		// Eliminamos empleados duplicados
		eliminarEmpleadosDuplicados(empleados);
		
		// Cambiamos el nombre a los campos
		for (int i = 0; i < empleados.size(); i++) {
			for (int j = 0; j < empleados.get(i).getDatosEmpleado().size(); j++) {
				String nombreCampo = empleados.get(i).getDatosEmpleado().get(j).getNombreAtributo().toString();
				
		    	// Se sustituye el nombre por el que se le quiere dar en el XML
				switch (nombreCampo) {
				case "Apellido1":
					nombreCampo = "PrimerApellido";
					break;
				case "Apellido2":
					nombreCampo = "SegundoApellido";
					break;
				case "Nombre empresa":
					nombreCampo = "Empresa";
					break;
				}
		    	
				empleados.get(i).getDatosEmpleado().get(j).setNombreAtributo(nombreCampo);
			}
		}
		
		return empleados;
	}
	
	/**
	 * Devuelve una nueva copia (con otra referencia distinta) del ArrayList<Empleado> <strong>listaEmpleados</strong> pasado como par�metro.
	 * @param listaEmpleados
	 * @return
	 */
	public static ArrayList<Empleado> clonaLista (ArrayList<Empleado> listaEmpleados) {
		ArrayList<Empleado> listaNueva = new ArrayList<Empleado>();
		
		for (int i = 0; i < listaEmpleados.size(); i++) {
			Empleado emp = new Empleado(listaEmpleados.get(i).getNif(), listaEmpleados.get(i).getNumeroFila(), listaEmpleados.get(i).getEmail(),listaEmpleados.get(i).getCuentaBancaria(), listaEmpleados.get(i).getIBAN());
			
			ArrayList<AtributoEmpleado> nuevaLista = new ArrayList<AtributoEmpleado>();
			
			for (int x = 0; x < listaEmpleados.get(i).getListaAtributosEmpleado().size(); x++)
				nuevaLista.add(new AtributoEmpleado(listaEmpleados.get(i).getListaAtributosEmpleado().get(x).getNombreAtributo(),
						listaEmpleados.get(i).getListaAtributosEmpleado().get(x).getValorAtributo()));
			
			emp.setListaAtributosEmpleado(nuevaLista);
			listaNueva.add(emp);
		}
		
		return listaNueva;
	}
	
	public static void setEmpleadosSalarioBase(ArrayList<Empleado> listaEmpleados, ArrayList<SalarioCategoria> listaSalariosCategoria) {
		Empleado empleadoAux;
		for(int i=0; i<listaEmpleados.size(); i++) {
			empleadoAux = listaEmpleados.get(i);
			setSalarioBase(empleadoAux, listaSalariosCategoria);
		}
	}
	
	public static void setSalarioBase(Empleado empleado, ArrayList<SalarioCategoria> listaSalariosCategoria) {
		String categoriaEmpleado = empleado.getCategoria();
		SalarioCategoria salarioCatAct;
		for(int i=0; i<listaSalariosCategoria.size(); i++) {
			salarioCatAct = listaSalariosCategoria.get(i);
			if(categoriaEmpleado.equalsIgnoreCase(salarioCatAct.getCategoria())) {
				//La categoria es la actual
				empleado.setSalarioCategoria(salarioCatAct);
				return;
			}
		}
		System.err.println("ERROR Se ha indicado una categoria para un empleado que no corresponde a ninguna categoria listadada en el EXCEL. Empleado:"+empleado.getNif()+" en fila: "+empleado.getNumeroFila());
	}
	
	public static void formateaProrrateo(ArrayList<Empleado> listaEmpleados) {
		for(int i=0; i<listaEmpleados.size(); i++) {
			formateaProrrateo(listaEmpleados.get(i));
		}
	}
	
	public static void formateaProrrateo(Empleado empleado) {
		String prorrateo = (String) empleado.getAtributo("ProrrataExtra").getValorAtributo();
		if(prorrateo.equalsIgnoreCase("SI")) {
			empleado.setProrrateo(true);
		}else {
			empleado.setProrrateo(false);
		}
	}
	
	public static void calculaTrienio(ArrayList<Empleado> listaEmpleados, Fecha fecha) {
		for(int i=0; i<listaEmpleados.size(); i++) {
			calculaTrienio(listaEmpleados.get(i), fecha);
		}
	}
	
	public static void calculaTrienio(Empleado empleado, Fecha fecha) {
		int mesCambioTrienio = mesCambioTrienio(empleado.getFechaEntrada(), fecha);
		//TODO Revisar
		empleado.setMesCambioTrienio(mesCambioTrienio);
		empleado.setTrienioAnterior(getTrienioAnt(empleado, fecha));
		empleado.setTrienioSiguiente(getTrienioSig(empleado, fecha, mesCambioTrienio));
		/*
		System.out.println("Fecha calculo: "+fecha.toString());
		System.out.println("Empleado: "+empleado.getNombre()+" "+empleado.getApellido1()+" Fecha entrada="+empleado.getFechaEntrada().toString());
		System.out.println("Cambio de trienio:"+mesCambioTrienio);
		if(empleado.getTrienioAnterior()!=null)
			System.out.println("Trienio Ant: "+empleado.getTrienioAnterior().getNumTrienio());
		if(empleado.getTrienioSiguiente() !=null)
			System.out.println("Trienio Sig: "+empleado.getTrienioSiguiente().getNumTrienio());
		System.out.println("__________________________________");
		*/
	}
	
	/*private static Trienio getTrienioSig(Empleado empleado, Fecha fecha, int mesCambioTrienio) {
		Trienio trienio;
		if(mesCambioTrienio == -1) {
			//Si este a�o no toca cambio nos da igual el siguiente
			return null;
		}
		Fecha fechaEntrada = empleado.getFechaEntrada();
		int difA�os = fecha.getAnno() - fechaEntrada.getAnno();
		int trienios = difA�os/3;
		if(mesCambioTrienio <= fecha.getMes()) {
			//Ya ha pasado el mes de cambio. por tanto el trienio siguiente ser� 1 mas de lo normal
			// Es un array, hay que acceder al valor-1
			trienio = Nucleo.getInstancia().getListaTrienios().get(trienios - 1);
			return trienio;
		}

		//Cuidado, si el siguiente es el primer trienio, no hay anterior, es null
		if(trienios == 0) {
			return null;
		}
		trienio = Nucleo.getInstancia().getListaTrienios().get(trienios-1);
		return trienio;
	}*/
	
	private static Trienio getTrienioSig(Empleado empleado, Fecha fecha, int mesCambioTrienio) {
		Fecha fechaEntrada = empleado.getFechaEntrada();
		int difAnos = fecha.getAnno() - fechaEntrada.getAnno();
		int posicion = difAnos / 3;
		
		// No cobra ning�n trienio ni hay cambio de trienio
		if (difAnos < 3)
			return null;
		// No cobraba trienio y pasa a cobrar el primer trienio
		// Cobraba ya alg�n trienio y hay cambio de trienio
		if (difAnos % 3 == 0){
			if(fechaEntrada.getMes() == 12)
				return null;
			else
				return Nucleo.getInstancia().getListaTrienios().get(posicion - 1);
		// El trienio que sigue cobrando est� en trienioAnterior porque no hay cambio de trienio
		}else if(difAnos%3 == 1 && fechaEntrada.getMes()==12){
			return Nucleo.getInstancia().getListaTrienios().get(posicion - 1);
		}else{
			return null;
		}
	}

	/*private static Trienio getTrienioAnt(Empleado empleado, Fecha fecha) {
		Fecha fechaEntrada = empleado.getFechaEntrada();
		int difA�os = fecha.getAnno() - fechaEntrada.getAnno();
		if(difA�os%3 == 0 && fecha.getMes()<fechaEntrada.getMes()) {
			//Si todavia no se ha completado el a�o la dif ser� uno menos
			difA�os--;
			System.out.println("Dif de a�os:"+difA�os);
		}
		int trienios = difA�os/3;
		//TODO
		//Cuidado con si los a�os ya se han cumplido o no
		//Cuidado, si el siguiente es el primer trienio, no hay anterior, es null
		Trienio trienio;
		if(trienios == 0) {
			return null;
		}
		if(trienios == 1) {
			
			//Comprobar si ya ha cumplido ese trienio o no
			if(difA�os<=3) {
				return null;
			}
			//Si el anterior es el primero, comprobar si 
			trienio = Nucleo.getInstancia().getListaTrienios().get(trienios-1);
			return trienio;
		}
		
		trienio = Nucleo.getInstancia().getListaTrienios().get(trienios-1);
		return trienio;
	}*/
	
	private static Trienio getTrienioAnt(Empleado empleado, Fecha fecha) {
		Fecha fechaEntrada = empleado.getFechaEntrada();
		int difAnos = fecha.getAnno() - fechaEntrada.getAnno();
		int posicion = difAnos / 3;
		// No cobra ning�n trienio ni hay cambio de trienio
		// No cobraba trienio y pasa a cobrar el primer trienio
		if (difAnos <= 3)
			return null;
		// Cobraba ya alg�n trienio y hay cambio de trienio
		if (difAnos % 3 == 0){
			
			if(fechaEntrada.getMes() == 12){
				return Nucleo.getInstancia().getListaTrienios().get(posicion - 1); 
			}else{
				return Nucleo.getInstancia().getListaTrienios().get(posicion - 2);
			}
		// Cobraba ya alg�n trienio y lo sigue cobrando
		}else{
			return Nucleo.getInstancia().getListaTrienios().get(posicion - 1);
		}
	}
	
	public static int mesCambioTrienio(Fecha fechaEntrada, Fecha fechaCalcular) {
		if((((fechaCalcular.getAnno()-fechaEntrada.getAnno())%3 == 1) && (fechaEntrada.getMes()==12)) && (fechaCalcular.getAnno()-fechaEntrada.getAnno())>=3){
			return 1;
		}
		if((fechaCalcular.getAnno()-fechaEntrada.getAnno())%3 == 0) {
			if((fechaCalcular.getAnno()-fechaEntrada.getAnno())>=3) {
				//Hay cambio de trienio en este a�o
				int mes = fechaEntrada.getMes()+1;
				if(mes > 12) {
					//Si el trabajador entro en diciembre, el mes de cambio de fecha ser� 13, que es enero del a�o siguiente
					return -1;
				}
				return mes;
			}
		}
		return -1;
	}
	
	public static double getValorRetencion (double brutoAnual) {
		ArrayList<Retencion> listaRetenciones = Nucleo.getInstancia().getListaRetenciones();
		Retencion retencionAct;
		for(int i=0; i<listaRetenciones.size(); i++) {
			retencionAct = listaRetenciones.get(i);
			double valorRetencion = retencionAct.getBruto();
			if(brutoAnual<=valorRetencion) {
				//Este es el valor de la retencion que le corresponde
				return retencionAct.getPorcentajeRetencion();
			}
		}
		//No deberia devolver nunca -1
		return -1;
	}
	
	public static String getNombreFicheroNomina(Empleado empleado, Fecha fecha) {
    	String nombreArchivo = "";
    	nombreArchivo += empleado.getNif() + "_" + empleado.getNombre() + empleado.getApellido1();
    	if(empleado.getApellido2() != null) {
    		nombreArchivo += empleado.getApellido2();
    	}
    	nombreArchivo += "_"+mesToString(fecha.getMes());
    	nombreArchivo += fecha.getAnno();
		return nombreArchivo;
	}
	
	public static String mesToString(int mes) {
		switch (mes) {
			case 1:
				return "Enero";
			case 2:
				return "Febrero";
			case 3:
				return "Marzo";
			case 4:
				return "Abril";
			case 5:
				return "Mayo";
			case 6:
				return "Junio";
			case 7:
				return "Julio";
			case 8:
				return "Agosto";
			case 9:
				return "Septiembre";
			case 10:
				return "Octubre";
			case 11:
				return "Noviembre";
			case 12:
				return "Diciembre";
		}
		return "MES NO VALIDO";
	}
	
	
	 public static ArrayList<Empresas> getListaEmpresas(ArrayList<Empleado> listaEmpleados){
	        ArrayList<Empresas> listaEmpresas = new ArrayList<Empresas>();
	        for(int i=0; i<listaEmpleados.size(); i++) {
	            insertaEmpresa(listaEmpresas, listaEmpleados.get(i));
	        }
	        return listaEmpresas;
	    }
	   
    public static void insertaEmpresa(ArrayList<Empresas> listaEmpresas, Empleado empleado) {
        //Comprueba si la empresa ya estaba en la lista
        Empresas empresaAux;
        for(int i=0; i<listaEmpresas.size(); i++) {
            empresaAux = listaEmpresas.get(i);
            if(empresaAux.getCif() == empleado.getCifEmpresa()){
                //Si la empresa ya estaba hace return
                return;
            }
        }
        //Si acaba el bucle la empresa no estaba la inserta y hace return
        listaEmpresas.add(new Empresas(empleado.getEmpresa(), empleado.getCifEmpresa()));
    }
    
    public static void eliminaEmpleadosIncorrectos(ArrayList<Empleado> listaEmpleados, ArrayList<Empleado> empleadosErrores) {
    	//Elimia los empleados incorrectos o repetidos de la lista
    	Empleado empleadoAEliminar;
    	for(int i=0; i<empleadosErrores.size(); i++) {
    		empleadoAEliminar = empleadosErrores.get(i);
    		for(int j=0; j<listaEmpleados.size(); j++) {
    			if(listaEmpleados.get(j).getNumeroFila() == empleadoAEliminar.getNumeroFila()) {
    				//El empleado a eliminar es el de la posicion J
    				listaEmpleados.remove(j);
    			}
    		}
    	}
    }
}
