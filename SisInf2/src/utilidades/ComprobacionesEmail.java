package utilidades;

import java.util.ArrayList;

import objetos.Empleado;
import objetos.PrefijoEmail;

public class ComprobacionesEmail {
	static ArrayList<PrefijoEmail> listaPrefijosEmail = new ArrayList<PrefijoEmail>();
	static ArrayList<String> emailsUsados = new ArrayList<>();

	
	private static void compruebaEmailsExistentes(ArrayList<Empleado> empleados) {
		Empleado empleadoAux;
		for(int i=0; i<empleados.size(); i++) {
			empleadoAux = empleados.get(i);
			String emailAux = empleadoAux.getEmail();
			if(emailAux!=null) {
				emailsUsados.add(emailAux);
				String prefijoConEmpresa = emailAux.substring(0, emailAux.indexOf("@")-2) + "-" + emailAux.substring(emailAux.indexOf("@")+1, emailAux.length()-3);
				//System.out.println("Prefijo con Empresa = "+prefijoConEmpresa);
				PrefijoEmail prefijoAux = new PrefijoEmail(prefijoConEmpresa);
				listaPrefijosEmail.add(prefijoAux);
			}
		}
	}
	
	/**
	 * Genera el correo electr�nico (email) correspondiente a cada empleado del <strong>ArrayList<Empleado> empleadosOginales</strong> y lo almacena en su correspondiente empleado en el <strong>ArrayList<Empleado> empleadosCorregidos</strong>.
	 * @param empleadosOriginales Los empleados originalmente leidos del Excel
	 * @param empleadosCorregidos Los empleados con los cambios ya realizados
	 */
	public static void completaEmailsEmpleados(ArrayList<Empleado> empleadosOriginales, ArrayList<Empleado> empleadosCorregidos) {
		compruebaEmailsExistentes(empleadosOriginales);
		Empleado empleadoAux;
		for (int i = 0; i < empleadosOriginales.size(); i++) {
			empleadoAux = empleadosOriginales.get(i);
			if(empleadoAux.getEmail()==null) {
				String emailEmpleado = generaEmail(empleadoAux);
				Empleado empleadoDeCorregidos = getEmpleadoDeCorregidos(empleadosCorregidos, empleadoAux);
				empleadoDeCorregidos.addAtributo("Email", emailEmpleado);
			}
		}
	}
	
	private static Empleado getEmpleadoDeCorregidos(ArrayList<Empleado> empleadosCorregidos, Empleado empleadoOriginal) {
		Empleado aux;
		for(int i=0; i<empleadosCorregidos.size();i++) {
			aux = empleadosCorregidos.get(i);
			if(aux.equals(empleadoOriginal)) {
				return aux;
			}
		}
		empleadosCorregidos.add(empleadoOriginal);
		return empleadoOriginal;
	}
	
	/**
	 * Genera el correo electr�nico (email) correspondiente al <strong>Empleado empleado</strong>.
	 * @param empleado
	 * @return
	 */
	public static String generaEmail(Empleado empleado) {
		String nombre = empleado.getNombre();
		String apellido1 = empleado.getApellido1();
		String apellido2 = empleado.getApellido2();
		
		// Solo se puede generar si posee nombre y primer apellido, al menos
		if (nombre == null || apellido1 == null ) {
			return null;
		}
		
		String prefijo = "";
		prefijo += nombre.substring(0, 3).toLowerCase();
		prefijo += apellido1.substring(0, 2).toLowerCase();
		if (apellido2 != null && apellido2 != "") {
			prefijo = prefijo + apellido2.substring(0,2).toLowerCase();
		}
		prefijo = utilidades.Utilidades.eliminaTildes(prefijo);
		//	System.out.println("Nombre: "+nombre);
		//	System.out.println("Apellido: "+apellido1);
		//	System.out.println("Apellido2: "+apellido2);
		//	System.out.println("Prefijo: "+prefijo);
		
		// Hay que implementar que el prefijo debe aumentar solo si existe otro en la misma empresa
		String prefijoConEmpresa = prefijo + "-" + empleado.getEmpresa();
		String numEmail = getNumEmail(prefijoConEmpresa);
		String empresa = empleado.getEmpresa();
		String email = prefijo + numEmail + "@" + empresa + ".es";
		//	System.out.println("\n\n");
		if(emailsUsados.contains(email)) {
			email = prefijo + getNumEmail(prefijoConEmpresa) + "@" + empresa + ".es";
		}
		//System.out.println("Se ha generado el Email: " + email);
		return email;
	}
	
	private static void aumentaNumEmail(String prefijoConEmpresa) {
		PrefijoEmail prefijoAux;
		String textoPrefijoAux;
		for(int i=0; i<listaPrefijosEmail.size();i++) {
			prefijoAux=listaPrefijosEmail.get(i);
			textoPrefijoAux=prefijoAux.getNombre();
			if(textoPrefijoAux.equalsIgnoreCase(prefijoConEmpresa)) {
				prefijoAux.addEntrada();
			}
		}
	}
	
	/**
	 * Obtiene el n�mero de sufijo correspondiente al <strong>String prefijoEntrada</strong> dependiendo del n�mero de empleados con ese mismo prefijo.
	 * @param prefijoEntrada
	 * @return
	 */
	private static String getNumEmail(String prefijoConEmpresa) {
		PrefijoEmail prefijoAux;
		String nombre;
		for (int i = 0; i < listaPrefijosEmail.size(); i++) {
			prefijoAux = listaPrefijosEmail.get(i);
			nombre = prefijoAux.getNombre();
			if (nombre.equalsIgnoreCase(prefijoConEmpresa)) {
				// Ya estaba registrado
				// Aumentamos el registro y devolvemos el numero
				String numero = prefijoAux.getEntrada();
				prefijoAux.addEntrada();
				if (numero.length() == 1) {
					numero = "0" + numero;
				}
				return numero;
			}
		}
		// Si acaba el bucle no estaba registrado
		// Lo registramos y devolvemos 00
		prefijoAux = new PrefijoEmail(prefijoConEmpresa);
		listaPrefijosEmail.add(prefijoAux);
		return "00";
	}

}
