package interfaz;

import java.awt.Color;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import principal.Nucleo;
import utilidades.Constantes;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UIPrincipal extends JFrame {
	
	private final static String ICONO_PATH = "resources/imagenes/iconos/iconoPrincipal_x32.png";
	
	public UIPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Sistemas de la Informaci\u00F3n II");
		setSize(400, 400);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ICONO_PATH));
		getContentPane().setLayout(null);
		
		
		JLabel lblFicheroExcel = new JLabel("Archivo Excel: "  + Constantes.FICHERO_EXCEL_NOMBRE);
		lblFicheroExcel.setHorizontalAlignment(SwingConstants.CENTER);
		lblFicheroExcel.setBounds(10, 11, 364, 14);
		getContentPane().add(lblFicheroExcel);
		
		JLabel lblFicheroXML = new JLabel("Archivo XML: " + Constantes.FICHERO_XML_ERRORES);
		lblFicheroXML.setHorizontalAlignment(SwingConstants.CENTER);
		lblFicheroXML.setBounds(10, 36, 364, 14);
		getContentPane().add(lblFicheroXML);
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 384, 61);
		lblFondo.setOpaque(true);
		lblFondo.setBackground(new Color(240, 230, 140));
		getContentPane().add(lblFondo);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 61, 384, 300);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton btnP1 = new JButton("Pr\u00E1ctica 1");
		btnP1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(null, "Haga clic en S� para ejecutar la pr�ctica 1.", "Pr�ctica 1", JOptionPane.YES_NO_OPTION) == 0)
					System.out.println("Ejecutar P1");
			}
		});
		btnP1.setBounds(12, 13, 360, 25);
		panel.add(btnP1);
		
		JButton btnP2 = new JButton("Pr\u00E1ctica 2");
		btnP2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(null, "Haga clic en S� para ejecutar la pr�ctica 2.", "Pr�ctica 2", JOptionPane.YES_NO_OPTION) == 0)
					System.out.println("Ejecutar P2");
			}
		});
		btnP2.setBounds(12, 51, 360, 25);
		panel.add(btnP2);
		
		JButton btnP3 = new JButton("Pr\u00E1ctica 3");
		btnP3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(null, "Haga clic en S� para ejecutar la pr�ctica 3.", "Pr�ctica 3", JOptionPane.YES_NO_OPTION) == 0)
					System.out.println("Ejecutar P3");
			}
		});
		btnP3.setBounds(12, 89, 360, 25);
		panel.add(btnP3);
		
		JButton btnP4 = new JButton("Pr\u00E1ctica 4");
		btnP4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(null, "Haga clic en S� para ejecutar la pr�ctica 4.", "Pr�ctica 4", JOptionPane.YES_NO_OPTION) == 0) {
					System.out.println("Ejecutar P4"); /*
					if (Nucleo.getInstancia().ejecutarPractica3())
						JOptionPane.showMessageDialog(null, "La pr�ctica 4 se ha ejecutado correctamente.", "Pr�ctica 4", JOptionPane.PLAIN_MESSAGE);
					else
						JOptionPane.showMessageDialog(null, "La pr�ctica 4 se ha ejecutado correctamente.", "Pr�ctica 4", JOptionPane.ERROR_MESSAGE);
						*/
				}
			}
		});
		btnP4.setBounds(12, 127, 360, 25);
		panel.add(btnP4);
		
		for (JButton boton : generarBotones())
			panel.add(boton);
		
		setVisible(true);
		setResizable(false);
	}
	
	private ArrayList<JButton> generarBotones() {
		ArrayList<JButton> botones = new ArrayList<JButton>();
		for (int i = 0; i < 9; i++) {
			JButton boton = new JButton();
			if (i == 0)
				boton.setText("Buscar duplicados");
			else
				boton.setText("Opci�n " + (i + 1));
			botones.add(boton);
		}
		return botones;
	}
}
